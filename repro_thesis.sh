#!/bin/bash

################# Solvers #################

create_solvers () {
    echo "Creating FM core solvers with linear"
    cd exp_code_ffm/solvers
    ./create_fm_core_solvers.sh
    cd ../..
    echo "Creating FM core solvers without linear"
    cd exp_code_ffm/solvers
    ./create_fm_core_solvers_no_linear.sh
    cd ../..
    echo "Compiling calc_loss"
    cd exp_code_ffm/util
    g++ -std=c++0x -Wshadow -Wconversion -Wall -O3 -o "calc_loss" "calc_loss.cpp"
    cd ../../
    echo "Making all models"
    cd exp_code_ffm
    make
    cd ..
}

################# Small Datasets #################
# 2GB

run_phishing () {
    echo 'Getting phishing data'
    wget -P exp_code_ffm/data/phishing http://archive.ics.uci.edu/ml/machine-learning-databases/00327/Training%20Dataset.arff
    echo 'Running phishing experiments'
    cd exp_code_ffm/moredata/phishing
    time ./run.phishing.final.sh
    cd ../../..
}

run_adult () {
    echo 'Getting adult data'
    wget -P exp_code_ffm/data/adult http://mlr.cs.umass.edu/ml/machine-learning-databases/adult/adult.data
    wget -P exp_code_ffm/data/adult http://mlr.cs.umass.edu/ml/machine-learning-databases/adult/adult.test
    echo 'Running adult experiments'
    cd exp_code_ffm/moredata/adult
    time ./run.adult.final.sh
    cd ../../..
}

run_rna () {
    echo 'Getting rna data'
    wget -P exp_code_ffm/data/rna https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/cod-rna
    wget -P exp_code_ffm/data/rna https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/cod-rna.t
    echo 'Running rna experiments'
    cd exp_code_ffm/moredata/rna
    time ./run.rna.final.sh
    cd ../../..
}

run_ijcnn () {
    echo 'Getting ijcnn data'
    wget -P ./exp_code_ffm/data/ijcnn https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/ijcnn1.t.bz2
    wget -P ./exp_code_ffm/data/ijcnn https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/ijcnn1.tr.bz2
    wget -P ./exp_code_ffm/data/ijcnn https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/ijcnn1.val.bz2
    bzip2 -dk ./exp_code_ffm/data/ijcnn/ijcnn1.t.bz2
    bzip2 -dk ./exp_code_ffm/data/ijcnn/ijcnn1.tr.bz2
    bzip2 -dk ./exp_code_ffm/data/ijcnn/ijcnn1.val.bz2
    echo 'Running ijcnn experiments'
    cd exp_code_ffm/moredata/ijcnn
    time ./run.ijcnn.final.sh
    cd ../../..
}

run_movielens_100K () {
    echo 'Getting movielens-100K data'
    wget -P exp_code_ffm/data/movielens-100K http://files.grouplens.org/datasets/movielens/ml-100k.zip
    cd exp_code_ffm/data/movielens-100K
    unzip ml-100k.zip
    cd ../../..
    echo 'Running movielens-100K experiments'
    cd exp_code_ffm/moredata/movielens-100K
    time ./run.movielens-100K.final.sh
    cd ../../..
}

run_movielens_100K_all () {
    echo 'Getting movielens-100K-all data'
    wget -P exp_code_ffm/data/movielens-100K-all http://files.grouplens.org/datasets/movielens/ml-100k.zip
    cd exp_code_ffm/data/movielens-100K-all
    unzip ml-100k.zip
    cd ../../..
    echo 'Running movielens-100K-all experiments'
    cd exp_code_ffm/moredata/movielens-100K-all
    time ./run.movielens-100K-all.final.sh
    cd ../../..
}

plots_small_dataset () {
    cd exp_code_ffm
    python ./plots.py
    cd ..
}

################# Large Datasets #################

run_criteo () {
    # 81GB
    echo 'Getting criteo data'
    wget -P ./exp_code_ffm/data/criteo https://s3-eu-west-1.amazonaws.com/kaggle-display-advertising-challenge-dataset/dac.tar.gz
    echo 'Running criteo experiments'
    cd exp_code_ffm/two_comp/criteo
    time ./run.criteo.final.sh
    python ./plots.py
    cd ../../..
}

run_avazu () {
    # 47GB
    echo 'Getting avazu data'
    cd exp_code_ffm/data/avazu
    kaggle competitions download -c avazu-ctr-prediction
    cd ../../..
    echo 'Running avazu experiments'
    cd exp_code_ffm/two_comp/avazu
    time ./run.avazu.final.sh
    python ./plots.py
    cd ../../..
}

################# Thesis #################

create_thesis () {
    cd exp_code_ffm
    python ensemble_improvement.py
    cd ..
    cd latex
    pdflatex tesis.tex
    bibtex tesis
    pdflatex tesis.tex
    pdflatex tesis.tex
    cd ..
}

################# Run all #################
time create_solvers 2>&1 | tee log_create_solvers
time run_phishing 2>&1 | tee log_run_phishing
time run_adult 2>&1 | tee log_run_adult
time run_rna 2>&1 | tee log_run_rna
time run_ijcnn 2>&1 | tee log_run_ijcnn
time run_movielens_100K 2>&1 | tee log_run_movielens_100K
time run_movielens_100K_all 2>&1 | tee log_run_movielens_100K_all
time plots_small_dataset 2>&1 | tee log_plots_small_dataset
time run_criteo 2>&1 | tee log_run_criteo
time run_avazu 2>&1 | tee log_run_avazu
time create_thesis 2>&1 | tee log_create_thesis

# thesis-fms

This repo has the code used for my Master's Thesis (`latex/tesis_v5.pdf`).

Running the `repro_thesis.sh` script will run all experiments in the thesis and re-compile the thesis with the new results to `latex/tesis.pdf`

## Memory/CPU requirements

Make sure you have at least 150GB free disk space and 60GB RAM. You'll need (at least) 8 CPUs.

## Set up your Kaggle credentials

Make sure you have the credentials set up for submitting to the Kaggle platform. See 
[here](https://github.com/Kaggle/kaggle-api) for how to do this. For me, this means my `$HOME/.kaggle/kaggle.json` file looks like this:

`{"username":"sprillo","key":"KEY_I_WONT_TELL_YOU"}`

## Running the experiments

We ran our experiments inside a docker container. The docker image was built from the `Dockerfile` file in this repo. Since package versions were (unfortunately) not pinned, building a new image out of the Dockerfile and running the `repro_thesis.sh` script from inside it does not guarantee that everything will work for you (although it probably will). For this reason, we have pushed the docker image used to run the experiments to dockerhub so you can use it too.

### Get the docker image

Pull the docker image used to run the experiments:

```
$ docker pull sprillo/thesis-fms:1
```

### Run the docker image

Create a persistent working directory for your docker image:

```
$ mkdir $HOME/thesis-fms
```

Asuming your Kaggle credentials are stored in `$HOME/.kaggle/kaggle.json`, run the image with:

```
$ docker run -it -v $HOME/thesis-fms:/home/condauser/data -v ~/.kaggle:/home/condauser/.kaggle sprillo/thesis-fms:1 /bin/bash
```

(These arguments will allow the container to see your Kaggle credentials and bind `$HOME/thesis-fms` to `/home/condauser/data` so data persists after the container is gone.)

### Clone this repo

Now you're inside the container, first clear the http proxies (the dockerfile set these up because we needed them - probably a bad idea as now you have to clear them instead):

```
$ http_proxy=
$ https_proxy=
```

cd to the persistent directory:

```
$ cd data/
```

Clone this repo:

```
$ git clone https://sebaprillo@bitbucket.org/sebaprillo/thesis-fms.git
```

### Remove our results

Because this repo contains all the output from our experiments, to make sure you are able to reproduce everything we recommend you to remove all our output files. For the small datasets, (at `thesis-fms/exp_code_ffm/moredata/{adult|ijcnn|movielens-100K|movielens-100K-all|phishing|rna}`) you will want to:
```
rm -r grid_search_results* *.png
```
and for avazu and criteo (at `thesis-fms/exp_code_ffm/two_comp/{avazu|criteo}`) you will want to:
```
rm -r output
```

### Run all experiments

Run all experiments and re-compile the thesis with the new results by doing:

```
$ cd thesis-fms/
$ ./repro_thesis.sh
```

Running all experiments takes about 5 full days on an AWS EC2 r4.2xlarge instance.

### Selectively running experiments

If you want to selectively run some experiments, do this by commenting out the experiments you don't want at the end of the `repro_thesis.sh` script. For example, if you only wanted to reproduce the Avazu experiments:

```
################# Run all #################
time create_solvers 2>&1 | tee log_create_solvers
# time run_phishing 2>&1 | tee log_run_phishing
# time run_adult 2>&1 | tee log_run_adult
# time run_rna 2>&1 | tee log_run_rna
# time run_ijcnn 2>&1 | tee log_run_ijcnn
# time run_movielens_100K 2>&1 | tee log_run_movielens_100K
# time run_movielens_100K_all 2>&1 | tee log_run_movielens_100K_all
# time plots_small_dataset 2>&1 | tee log_plots_small_dataset
# time run_criteo 2>&1 | tee log_run_criteo
time run_avazu 2>&1 | tee log_run_avazu
time create_thesis 2>&1 | tee log_create_thesis
```

Of course, you can be even more selective and only run some experiments for each dataset, e.g. for Avazu, comment out the experiments your don't want from the `exp_code_ffm/two_comp/avazu/run.avazu.final.sh` file.

### Troubleshooting

If some bash command fails while running `repro_thesis.sh` (e.g. because of network issues), there isn't much to do: comment out from the script what you were able to run successfully, and re-run.

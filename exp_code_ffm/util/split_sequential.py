#!/usr/bin/env python3

import argparse, sys, random

parser = argparse.ArgumentParser()
parser.add_argument('ratio', type=float)
parser.add_argument('src', type=str)
parser.add_argument('tr', type=str)
parser.add_argument('va', type=str)
parser.add_argument('tr_size', type=int)
ARGS = vars(parser.parse_args())

with open(ARGS['tr'], 'w') as f_tr, open(ARGS['va'], 'w') as f_va:
    i = 0
    for line in open(ARGS['src']):
        if i < ARGS['ratio'] * ARGS['tr_size']:
            f_va.write(line)
        else:
            f_tr.write(line)
        i += 1

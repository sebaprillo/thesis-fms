#!/usr/bin/env python3

import argparse, sys

parser = argparse.ArgumentParser()
parser.add_argument('src', type=str)
parser.add_argument('dst', type=str)
ARGS = vars(parser.parse_args())

with open(ARGS['dst'], 'w') as f_dst:
    with open(ARGS['src']) as f_in:
        for line in f_in:
            tokens = line.strip().split()
            output = tokens[0]
            for token in tokens[1:]:
                field, feature, value = token.split(':')
                output += ' {0}:{1}'.format(feature, value)
            f_dst.write(output + '\n')

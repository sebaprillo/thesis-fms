#!/usr/bin/env python3

import argparse, sys, random, pandas, bisect

parser = argparse.ArgumentParser()
parser.add_argument('leaderboard_path', type=str)
parser.add_argument('score_file_path', type=str)
parser.add_argument('output_file_path', type=str)
ARGS = vars(parser.parse_args())

leaderboard_path = ARGS['leaderboard_path']
with open(ARGS['score_file_path']) as score_file:
    for line in score_file:
        score = float(line)
        break
#leaderboard_path = 'two_comp/avazu/avazu-ctr-prediction-publicleaderboard.csv'
df = pandas.read_csv(leaderboard_path).sort_values('Score').groupby('TeamName').first().sort_values('Score')
scores = df['Score']
position = bisect.bisect_left(scores, score) + 1
with open(ARGS['output_file_path'], 'w') as output_file:
    output_file.write(str(position) + '\n')


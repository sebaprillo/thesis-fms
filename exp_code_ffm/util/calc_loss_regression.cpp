#include <bits/stdc++.h>

using namespace std;

#define forn(i,n) for(int i = 0; i < int(n); i++)

int const kMaxLineSize = 100000;

inline int my_hash(int idx, int num_fields){
    return idx % num_fields;
}

int main(int argc, char **argv)
{
    //cout << "This script computes the loss of a pred file (one column only) wrt te.ffm file" << endl;
    assert(argc == 3);
    string pred_file_path = argv[1];
    const char *test_file_path = argv[2];
    ifstream pred_file(pred_file_path);
    FILE *test_file = fopen(test_file_path, "r");
    char line[kMaxLineSize];
    double loss = 0.0;
    int rows = 0.0;
    for(int i = 0; fgets(line, kMaxLineSize, test_file) != nullptr; i++, rows++)
    {
        //if(i % 10000 == 0) cerr << "Line " << i << " , accumulated loss:" << loss / i << endl;
        char *y_char = strtok(line, " \t");
        double y = atof(y_char);
        double pred;
        pred_file >> pred;
        loss += (y - pred) * (y - pred);
    }
    loss /= rows;
    loss = sqrt(loss);
    cerr << loss;
    fclose(test_file);
    pred_file.close();
}

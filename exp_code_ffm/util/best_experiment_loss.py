#!/usr/bin/env python3

import argparse, sys, pickle, os

if len(sys.argv) == 1:
    sys.argv.append('-h')

parser = argparse.ArgumentParser(description='process some integers')
parser.add_argument('experiment_name', type=str)
parser.add_argument('public_or_private', type=str)
parser.add_argument('dst_path', type=str)
args = vars(parser.parse_args())

assert(args['public_or_private'] in ['public', 'private'])

res = 1e9

for filename in os.listdir('output/' + args['experiment_name']):
    #print("processing file " + filename)
    if args['public_or_private'] in filename:
        with open('output/' + args['experiment_name'] + '/' + filename, "r") as f:
            for line in f:
                res = min(res, float(line))

with open('output/' + args['dst_path'], "w") as f:
    f.write("%.5f\n" %res)

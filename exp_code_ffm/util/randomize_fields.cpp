#include <bits/stdc++.h>

using namespace std;

#define forn(i,n) for(int i = 0; i < int(n); i++)

int const kMaxLineSize = 100000;

inline int my_hash(int idx, int num_fields){
    return idx % num_fields;
}

int main(int argc, char **argv)
{
    cout << "This script randomizes the fields in a ffm file" << endl;
    assert(argc == 4);
    const char *input_file_path = argv[1];
    string output_file_path = argv[2];
    int num_fields = atoi(argv[3]);
    FILE *f = fopen(input_file_path, "r");
    ofstream f_out(output_file_path);
    char line[kMaxLineSize];
    for(int i = 0; fgets(line, kMaxLineSize, f) != nullptr; i++)
    {
        if(i % 10000 == 0) cerr << "Line " << i << endl;
        char *y_char = strtok(line, " \t");
        f_out << y_char;

        for(; ; )
        {
            char *field_char = strtok(nullptr,":");
            char *idx_char = strtok(nullptr,":");
            char *value_char = strtok(nullptr," \t");
            if(field_char == nullptr || *field_char == '\n')
                break;

            int field = atoi(field_char);
            int idx = atoi(idx_char);
            double value = atof(value_char);
            f_out << " " << my_hash(idx, num_fields) << ":" << idx << ":" << value;
        } f_out << "\n";
    }
    fclose(f);
    f_out.close();
}

symlink_utils () {
    echo "Symlinking utils"
    cd ../../util
    g++ -std=c++0x -Wshadow -Wconversion -Wall -O3 -o "calc_loss" "calc_loss.cpp"
    g++ -std=c++0x -Wshadow -Wconversion -Wall -O3 -o "calc_loss_regression" "calc_loss_regression.cpp"
    cd ../moredata/${dataset_name}
    #cd ../../util
    #g++ -std=c++0x -Wshadow -Wconversion -Wall -O3 -o "randomize_fields" "randomize_fields.cpp"
    #cd ../moredata/${dataset_name}
    ln -sf ../../util/best.py
    ln -sf ../../util/last.py
    ln -sf ../../util/grid_para3.py
    ln -sf ../../util/merge_prediction.py
    ln -sf ../../util/merge_prediction_regression.py
    ln -sf ../../util/unpickle_prediction.py
    ln -sf ../../util/calc_loss
    ln -sf ../../util/calc_loss_regression
}

ensemble_pos_and_neg_fm () {
    echo "ensembling ${1} and ${2} to ${3} and testing on ${4} with order ${5}"
    ./fm_w_linear_manual_ensembler $1.model $2.model $3.model
    touch $3.cmd
    touch $3.loss
    touch $3.train_log
    ../../solvers/fm${5}_w_linear_neutral/fm${5}_w_linear_neutral-predict $4 $3.model $3.pred 2>&1 | tee $3.loss
}

wait_for_file () {
    while true
    do
        if [ ! -f $1 ]; then
            echo "waiting for file ${1}..."
            sleep 10
            continue
        fi
        echo "done!"
        break
    done
}

final_ensembles () {
    for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
    do
        wait_for_file grid_search_results/fm${order}_w_linear.pred
        wait_for_file grid_search_results/fm${order}_w_linear_reversed.pred
        ./merge_prediction.py grid_search_results/fm${order}_w_linear.pred grid_search_results/fm${order}_w_linear_reversed.pred grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl
        ./unpickle_prediction.py grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred
        rm grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl
        ./calc_loss grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred te.ffm 2>&1 | tee grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.loss
    done
    #for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
    #do
    #    wait_for_file grid_search_results_early_stop_test/fm${order}_w_linear.pred
    #    wait_for_file grid_search_results_early_stop_test/fm${order}_w_linear_reversed.pred
    #    ./merge_prediction.py grid_search_results_early_stop_test/fm${order}_w_linear.pred grid_search_results_early_stop_test/fm${order}_w_linear_reversed.pred grid_search_results_early_stop_test/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl
    #    ./unpickle_prediction.py grid_search_results_early_stop_test/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl grid_search_results_early_stop_test/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred
    #    rm grid_search_results_early_stop_test/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl
    #    ./calc_loss grid_search_results_early_stop_test/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred te.ffm 2>&1 | tee grid_search_results_early_stop_test/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.loss
    #done
}

final_ensembles_regression () {
    for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
    do
        wait_for_file grid_search_results/fm${order}_w_linear.pred
        wait_for_file grid_search_results/fm${order}_w_linear_reversed.pred
        ./merge_prediction_regression.py grid_search_results/fm${order}_w_linear.pred grid_search_results/fm${order}_w_linear_reversed.pred grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl
        ./unpickle_prediction.py grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred
        rm grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl
        ./calc_loss_regression grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred te.ffm 2>&1 | tee grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.loss
    done
}

final_ensembles_regression_ffm () {
    wait_for_file grid_search_results/ffm_w_linear.pred
    wait_for_file grid_search_results/ffm_w_linear_reversed.pred
    ./merge_prediction_regression.py grid_search_results/ffm_w_linear.pred grid_search_results/ffm_w_linear_reversed.pred grid_search_results/ffm_w_linear_AND_ffm_w_linear_reversed.pred.pkl
    ./unpickle_prediction.py grid_search_results/ffm_w_linear_AND_ffm_w_linear_reversed.pred.pkl grid_search_results/ffm_w_linear_AND_ffm_w_linear_reversed.pred
    rm grid_search_results/ffm_w_linear_AND_ffm_w_linear_reversed.pred.pkl
    ./calc_loss_regression grid_search_results/ffm_w_linear_AND_ffm_w_linear_reversed.pred te.ffm 2>&1 | tee grid_search_results/ffm_w_linear_AND_ffm_w_linear_reversed.loss
}

final_ensembles_regression_parameterized () {
    for order in ${FINAL_EMSEMBLES_REGRESSION_PARAMETERIZED_ORDERS[*]}
    do
        wait_for_file grid_search_results/fm${order}_w_linear.pred
        wait_for_file grid_search_results/fm${order}_w_linear_reversed.pred
        ./merge_prediction_regression.py grid_search_results/fm${order}_w_linear.pred grid_search_results/fm${order}_w_linear_reversed.pred grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl
        ./unpickle_prediction.py grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred
        rm grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred.pkl
        ./calc_loss_regression grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.pred te.ffm 2>&1 | tee grid_search_results/fm${order}_w_linear_AND_fm${order}_w_linear_reversed.loss
    done
}

train_fm () {
    experiment_name=aux
    mkdir ${experiment_name}
    mkdir output
    mkdir output/${experiment_name}
    solver=fm2p0_w_linear
    solver_train="../../solvers/${solver}/${solver}-train"
    solver_predict="../../solvers/${solver}/${solver}-predict"
    auto_stop_log="${experiment_name}/auto_stop.${solver}.log"
    auto_stop_model="${experiment_name}/${solver}.model"
    auto_stop_pred="${experiment_name}/${solver}.base_va_pred"
    log="output/${experiment_name}/${solver}.train_log"
    model="${experiment_name}/${solver}.model"
    pred="${experiment_name}/${solver}.pred"
    loss="${experiment_name}/${solver}.loss"
    params="--task ${task} -l ${l} -r ${r} -k ${k} --sigma ${sigma}"
    ${solver_train} ${params} -t ${t} -p va.ffm tr.ffm ${auto_stop_model} 2>&1 | tee ${auto_stop_log}
    ${solver_predict} va.ffm ${auto_stop_model} ${auto_stop_pred} ${task}
    t=$(../../util/best.py ${auto_stop_log} | awk '{print $2}')
    echo "optimal iterations = ${t}"
    ${solver_train} ${params} -t ${t} -p te.ffm trva.ffm ${model} 2>&1 | tee ${log}
    ${solver_predict} te.ffm ${model} ${pred} ${task}
    ../../util/last.py ${log} | awk '{print $1}' > ${loss}
}

train_ffm () {
    experiment_name=aux
    mkdir ${experiment_name}
    mkdir output
    mkdir output/${experiment_name}
    solver=ffm_w_linear
    solver_train="../../solvers/${solver}/${solver}-train"
    solver_predict="../../solvers/${solver}/${solver}-predict"
    auto_stop_log="${experiment_name}/auto_stop.${solver}.log"
    auto_stop_model="${experiment_name}/${solver}.model"
    auto_stop_pred="${experiment_name}/${solver}.base_va_pred"
    log="output/${experiment_name}/${solver}.train_log"
    model="${experiment_name}/${solver}.model"
    pred="${experiment_name}/${solver}.pred"
    loss="${experiment_name}/${solver}.loss"
    params="--task ${task} -l ${l} -r ${r} -k ${k} --sigma ${sigma}"
    ${solver_train} ${params} -t ${t} -p va.ffm tr.ffm ${auto_stop_model} 2>&1 | tee ${auto_stop_log}
    ${solver_predict} va.ffm ${auto_stop_model} ${auto_stop_pred} ${task}
    t=$(../../util/best.py ${auto_stop_log} | awk '{print $2}')
    echo "optimal iterations = ${t}"
    ${solver_train} ${params} -t ${t} -p te.ffm trva.ffm ${model} 2>&1 | tee ${log}
    ${solver_predict} te.ffm ${model} ${pred} ${task}
    ../../util/last.py ${log} | awk '{print $1}' > ${loss}
}

train_linear () {
    experiment_name=aux
    mkdir ${experiment_name}
    mkdir output
    mkdir output/${experiment_name}
    solver=linear
    solver_train="../../solvers/${solver}/${solver}-train"
    solver_predict="../../solvers/${solver}/${solver}-predict"
    auto_stop_log="${experiment_name}/auto_stop.${solver}.log"
    auto_stop_model="${experiment_name}/${solver}.model"
    auto_stop_pred="${experiment_name}/${solver}.base_va_pred"
    log="output/${experiment_name}/${solver}.train_log"
    model="${experiment_name}/${solver}.model"
    pred="${experiment_name}/${solver}.pred"
    loss="${experiment_name}/${solver}.loss"
    params="--task ${task} -l ${l} -r ${r} -k ${k} --sigma ${sigma}"
    ${solver_train} ${params} -t ${t} -p va.ffm tr.ffm ${auto_stop_model} 2>&1 | tee ${auto_stop_log}
    ${solver_predict} va.ffm ${auto_stop_model} ${auto_stop_pred} ${task}
    t=$(../../util/best.py ${auto_stop_log} | awk '{print $2}')
    echo "optimal iterations = ${t}"
    ${solver_train} ${params} -t ${t} -p te.ffm trva.ffm ${model} 2>&1 | tee ${log}
    ${solver_predict} te.ffm ${model} ${pred} ${task}
    ../../util/last.py ${log} | awk '{print $1}' > ${loss}
}

train_poly2 () {
    experiment_name=aux
    mkdir ${experiment_name}
    mkdir output
    mkdir output/${experiment_name}
    solver=poly2_w_linear
    solver_train="../../solvers/${solver}/${solver}-train"
    solver_predict="../../solvers/${solver}/${solver}-predict"
    auto_stop_log="${experiment_name}/auto_stop.${solver}.log"
    auto_stop_model="${experiment_name}/${solver}.model"
    auto_stop_pred="${experiment_name}/${solver}.base_va_pred"
    log="output/${experiment_name}/${solver}.train_log"
    model="${experiment_name}/${solver}.model"
    pred="${experiment_name}/${solver}.pred"
    loss="${experiment_name}/${solver}.loss"
    params="--task ${task} -l ${l} -r ${r} -k ${k} --sigma ${sigma}"
    ${solver_train} ${params} -t ${t} -p va.ffm tr.ffm ${auto_stop_model} 2>&1 | tee ${auto_stop_log}
    ${solver_predict} va.ffm ${auto_stop_model} ${auto_stop_pred} ${task}
    t=$(../../util/best.py ${auto_stop_log} | awk '{print $2}')
    echo "optimal iterations = ${t}"
    ${solver_train} ${params} -t ${t} -p te.ffm trva.ffm ${model} 2>&1 | tee ${log}
    ${solver_predict} te.ffm ${model} ${pred} ${task}
    ../../util/last.py ${log} | awk '{print $1}' > ${loss}
}

train_libfm () {
    experiment_name=aux
    mkdir ${experiment_name}
    mkdir output
    mkdir output/${experiment_name}
    solver=../../solvers/libfm/bin/libFM
    auto_stop_log="${experiment_name}/auto_stop.libfm.log"
    log="output/${experiment_name}/libfm.train_log"
    loss="${experiment_name}/libfm.loss"
    params="-task ${task} -method ${method} -init_stdev ${init_stdev} -out ${out} -iter ${iter} -dim ${dim} -regular ${regular}"
    ${solver} ${params} -train tr.fm -test va.fm 2>&1 | tee ${auto_stop_log}
    iter=$(../../util/best_libfm.py ${auto_stop_log} | awk '{print $2}')
    echo "optimal iterations = ${iter}"
    ${solver} ${params} -train trva.fm -test te.fm 2>&1 | tee ${log}
    ../../util/last_libfm.py ${log} | awk '{print $1}' > ${loss}
}

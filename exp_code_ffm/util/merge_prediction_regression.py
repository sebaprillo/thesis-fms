#!/usr/bin/env python3

import argparse, csv, sys, pickle, collections, math

if len(sys.argv) == 1:
    sys.argv.append('-h')

parser = argparse.ArgumentParser(description='process some integers')
parser.add_argument('prd_paths', nargs='+', type=str)
parser.add_argument('out_path', type=str)
args = vars(parser.parse_args())

mprd = collections.defaultdict(list)
for path in args['prd_paths']:
    prd = open(path, 'r')
    for i, line in enumerate(prd):
        key = i
        value = float(line.split(',')[0])
        mprd[key].append(value)

for key in mprd:
    mprd[key] = sum(mprd[key])/len(mprd[key])

pickle.dump(mprd, open(args['out_path'], 'wb'))

#!/usr/bin/env python

import os, sys, subprocess

def find_last_logloss(out):
    last_logloss = 10000
    for each_iter in out.decode("utf-8").split('\n')[2:-1]:
        logloss = float(each_iter.split()[-2])
        last_logloss = logloss
    return last_logloss

def find_min_logloss(out):
    min_val = 10000
    min_iter = 10000
    for each_iter in out.decode("utf-8").split('\n')[2:-1]:
        num_iter = int(each_iter.split()[0])
        try:
            logloss = float(each_iter.split()[-2])
        except:
            print("loss is not a float")
            continue
        if logloss < min_val:
            min_val = logloss
            min_iter = num_iter
    return min_iter, min_val

solver = sys.argv[1]
tr_path = sys.argv[2]
te_path = sys.argv[3]

final_tr_path = sys.argv[4]
final_te_path = sys.argv[5]

output_dir = sys.argv[6]

l_list = list(map(float, sys.argv[7].split(',')))
k_list = list(map(float, sys.argv[8].split(',')))
r_list = list(map(float, sys.argv[9].split(',')))
t = int(sys.argv[10])
s = int(sys.argv[11])
assert(len(sys.argv) == 12 or len(sys.argv) == 13)
c_or_r = "c" if len(sys.argv) == 12 else sys.argv[12]

min_logloss = 10000
min_l = 0
min_k = 0
min_r = 0

curr = 0
tot = len(l_list) * len(k_list) * len(r_list)
for l in l_list:
    for k in k_list:
        for r in r_list:
            curr += 1
            sys.stdout.write('grid_search iteration: %d/%d. %s. ' %(curr, tot, solver))
            cmd = "../../solvers/{solver}/{solver}-train --task {c_or_r} -l {l} -s {s} -k {k} -r {r} -t {t} -p {te} {tr}".format(solver=solver, s=s, t = t, l = l, k = k, r = r, te = te_path, tr = tr_path, c_or_r = c_or_r)
            #print(cmd)
            proc = subprocess.Popen(cmd, shell=True, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
            out, err = proc.communicate()

            min_num_iter, min_logloss_in_out = find_min_logloss(out)

            if min_logloss_in_out < min_logloss:
                min_logloss = min_logloss_in_out
                min_l = l
                min_k = k
                min_r = r
                min_t = min_num_iter
            print('oracle:\t', min_logloss, 'now:\t', min_logloss_in_out, 'paras:\t l=', l, ' k=', k, ' r=', r, ' t=', min_t)

cmd = "../../solvers/{solver}/{solver}-train --task {c_or_r} -l {l} -s {s} -k {k} -r {r} -t {t} -p {te} {tr} {output_dir}/{solver}.model 2>&1 | tee {output_dir}/{solver}.train_log".format(solver=solver, t = min_t, l = min_l, k = min_k, r = min_r, te = final_te_path, tr = final_tr_path, output_dir = output_dir, s = s, c_or_r = c_or_r)
with open('{output_dir}/{solver}.cmd'.format(output_dir=output_dir, solver=solver), 'w') as out:
    out.write(cmd + '\n')
print(cmd)
proc = subprocess.Popen(cmd, shell=True, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
out, err = proc.communicate()
min_logloss_in_out = find_last_logloss(out)

with open('{output_dir}/best_params/{solver}.cmd.l'.format(solver=solver, output_dir=output_dir), 'a') as out:
    out.write(str(min_l))
with open('{output_dir}/best_params/{solver}.cmd.k'.format(solver=solver, output_dir=output_dir), 'a') as out:
    out.write(str(min_k))
with open('{output_dir}/best_params/{solver}.cmd.r'.format(solver=solver, output_dir=output_dir), 'a') as out:
    out.write(str(min_r))
with open('{output_dir}/best_params/{solver}.cmd.t'.format(solver=solver, output_dir=output_dir), 'a') as out:
    out.write(str(min_t))

cmd = "../../solvers/{solver}/{solver}-predict {te} {output_dir}/{solver}.model {output_dir}/{solver}.pred {c_or_r}".format(solver=solver, te = final_te_path, tr = final_tr_path, output_dir = output_dir, c_or_r = c_or_r)
with open('{output_dir}/{solver}.cmd'.format(solver=solver, output_dir=output_dir), 'a') as out:
    out.write(cmd)
print(cmd)
proc = subprocess.Popen(cmd, shell=True, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
out, err = proc.communicate()

output_path = output_dir + '/' + '{solver}.loss'.format(solver=solver)
with open(output_path, 'w') as out:
    out.write('%.5f'%min_logloss_in_out)

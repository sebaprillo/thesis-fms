#!/bin/bash

s=8

dataset_name=avazu
mkdir output
kaggle competitions leaderboard avazu-ctr-prediction -d
rm avazu-ctr-prediction-publicleaderboard.csv
unzip avazu-ctr-prediction.zip

symlink_utils () {
    echo "Symlinking utils"
    ln -sf ../../util/get_standing.py
}

generate_dataset () {
    #git clone https://github.com/guestwalk/kaggle-avazu.git #No longer clone, added it to repo
    cd kaggle-avazu
    make
    
    gunzip ../../../data/avazu/train.gz
    gunzip ../../../data/avazu/test.gz
    
    ln -sf ../../../data/avazu/train tr.r0.csv
    ./add_dummy_label.py ../../../data/avazu/test va.r0.csv
    
    cd base
    make -C mark/mark1 && ln -sf mark/mark1/mark1
    
    ./util/gen_data.py ../tr.r0.csv ../va.r0.csv tr.r0.app.new.csv va.r0.app.new.csv tr.r0.site.new.csv va.r0.site.new.csv
    ./util/parallelizer.py -s 12 ./converter/2.py tr.r0.app.new.csv va.r0.app.new.csv tr.r0.app.sp va.r0.app.sp
    ./util/parallelizer.py -s 12 ./converter/2.py tr.r0.site.new.csv va.r0.site.new.csv tr.r0.site.sp va.r0.site.sp
    
    cd ..
    cd ..
    
    ./sp2ffm.py kaggle-avazu/base/tr.r0.app.sp kaggle-avazu/base/tr.r0.app.sp.ffm
    ./sp2ffm.py kaggle-avazu/base/va.r0.app.sp kaggle-avazu/base/va.r0.app.sp.ffm
    ./sp2ffm.py kaggle-avazu/base/tr.r0.site.sp kaggle-avazu/base/tr.r0.site.sp.ffm
    ./sp2ffm.py kaggle-avazu/base/va.r0.site.sp kaggle-avazu/base/va.r0.site.sp.ffm

    what=app
    ln -sf kaggle-avazu/base/trtr.r0.${what}.sp.ffm ${what}.tr.ffm
    ln -sf kaggle-avazu/base/trva.r0.${what}.sp.ffm ${what}.va.ffm
    ln -sf kaggle-avazu/base/tr.r0.${what}.sp.ffm ${what}.trva.ffm
    ln -sf kaggle-avazu/base/va.r0.${what}.sp.ffm ${what}.te.ffm
    what=site
    ln -sf kaggle-avazu/base/trtr.r0.${what}.sp.ffm ${what}.tr.ffm
    ln -sf kaggle-avazu/base/trva.r0.${what}.sp.ffm ${what}.va.ffm
    ln -sf kaggle-avazu/base/tr.r0.${what}.sp.ffm ${what}.trva.ffm
    ln -sf kaggle-avazu/base/va.r0.${what}.sp.ffm ${what}.te.ffm
    what=none
    
    ../../util/split_sequential.py 0.2 kaggle-avazu/base/tr.r0.app.sp.ffm kaggle-avazu/base/trtr_chrono.r0.app.sp.ffm kaggle-avazu/base/trva_chrono.r0.app.sp.ffm 14596137
    ../../util/split_sequential.py 0.2 kaggle-avazu/base/tr.r0.site.sp.ffm kaggle-avazu/base/trtr_chrono.r0.site.sp.ffm kaggle-avazu/base/trva_chrono.r0.site.sp.ffm 25832830
}

submit_to_kaggle () {
    echo 'Submitting to Kaggle'
    cd kaggle-avazu/base/
    date=$(date)
    ../../sort_as_sample_submission.py ${experiment_name}/base.r0.prd.${solver}${suffix} ${experiment_name}/base.r0.prd.${solver}${suffix}_sorted ../../../../data/avazu/sampleSubmission.gz
    kaggle competitions submit -c avazu-ctr-prediction -f ${experiment_name}/base.r0.prd.${solver}${suffix}_sorted -m "Submit ${experiment_name}/${solver}${suffix} date=${date}"
    while true; do
        echo "Waiting for submission to be scored..."
        sleep 30
        kaggle competitions submissions --csv -c avazu-ctr-prediction | grep base > kaggle_submissions
        head -n 1 kaggle_submissions > ${experiment_name}/base.r0.prd.${solver}${suffix}.kaggle_submission
        status=$(awk -F "\"*,\"*" '{print $4}' ${experiment_name}/base.r0.prd.${solver}${suffix}.kaggle_submission)
        message=$(awk -F "\"*,\"*" '{print $3}' ${experiment_name}/base.r0.prd.${solver}${suffix}.kaggle_submission)
        if [ "$status" != "pending" ] && [ "$message" == "Submit ${experiment_name}/${solver}${suffix} date=${date}" ]; then
            echo "done!"
            break
        fi
    done
    awk -F "\"*,\"*" '{printf("%.5f\n", $5)}' ${experiment_name}/base.r0.prd.${solver}${suffix}.kaggle_submission > ../../output/${experiment_name}/${solver}${suffix}.public
    awk -F "\"*,\"*" '{printf("%.5f\n", $6)}' ${experiment_name}/base.r0.prd.${solver}${suffix}.kaggle_submission > ../../output/${experiment_name}/${solver}${suffix}.private
    cd ../..
    echo 'Computing public standing'
    ./get_standing.py avazu-ctr-prediction-publicleaderboard.csv output/${experiment_name}/${solver}${suffix}.public output/${experiment_name}/${solver}${suffix}.public_standing
}

train () {
    echo "${solver} -l ${l} -k ${k} -t ${t} -r ${r} -s ${s}, suffix=${suffix}, experiment_name=${experiment_name}"
    mkdir output/${experiment_name}
    mkdir kaggle-avazu/base/${experiment_name}
    echo "training app"
    ../../solvers/${solver}/${solver}-train -l ${l} -k ${k} -t ${t} -r ${r} -s ${s} app.trva.ffm kaggle-avazu/base/${experiment_name}/tr.r0.app.sp.${solver}${suffix}.model 2>&1 | tee output/${experiment_name}/avazu.app.${solver}${suffix}.log
    echo "training site"
    ../../solvers/${solver}/${solver}-train -l ${l} -k ${k} -t ${t} -r ${r} -s ${s} site.trva.ffm kaggle-avazu/base/${experiment_name}/tr.r0.site.sp.${solver}${suffix}.model 2>&1 | tee output/${experiment_name}/avazu.site.${solver}${suffix}.log
    echo "predicting app"
    ../../solvers/${solver}/${solver}-predict app.te.ffm kaggle-avazu/base/${experiment_name}/tr.r0.app.sp.${solver}${suffix}.model kaggle-avazu/base/${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd.no.id
    echo "predicting site"
    ../../solvers/${solver}/${solver}-predict site.te.ffm kaggle-avazu/base/${experiment_name}/tr.r0.site.sp.${solver}${suffix}.model kaggle-avazu/base/${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd.no.id
    echo "adding ids to app predictions"
    ./add_id_to_prd.py kaggle-avazu/base/va.r0.app.sp kaggle-avazu/base/${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd.no.id kaggle-avazu/base/${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd
    echo "adding ids to site predictions"
    ./add_id_to_prd.py kaggle-avazu/base/va.r0.site.sp kaggle-avazu/base/${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd.no.id kaggle-avazu/base/${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd
    echo "merging predictions"
    cd kaggle-avazu/base/
    ./util/pickle_prediction.py ${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd ${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd.pickle
    ./util/pickle_prediction.py ${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd ${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd.pickle
    ../../merge_prediction.py ${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd.pickle ${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd.pickle ${experiment_name}/va.r0.prd.pickle
    ./util/unpickle_prediction.py ${experiment_name}/va.r0.prd.pickle ${experiment_name}/base.r0.prd.${solver}${suffix}
    echo "Removing model files and intermediate prediction files"
    rm ${experiment_name}/*model ${experiment_name}/va.r0.*
    cd ../..
    echo 'Computing training time'
    train_time_app=$(tail output/${experiment_name}/avazu.app.${solver}${suffix}.log -n 1 | awk '{print $3}')
    train_time_site=$(tail output/${experiment_name}/avazu.site.${solver}${suffix}.log -n 1 | awk '{print $3}')
    echo "scale=0; ($train_time_app + $train_time_site)/1" | bc -l > output/${experiment_name}/${solver}${suffix}.time
    submit_to_kaggle
}

train_vary_t () {
    echo "${solver} -l ${l} -k ${k} -t ${t} -r ${r} -s ${s}, suffix=${suffix}, experiment_name=${experiment_name}"
    mkdir output/${experiment_name}
    mkdir kaggle-avazu/base/${experiment_name}
    t=$(../../util/best.py ${1} | awk '{print $2}')
    echo "training app, t = ${t}"
    ../../solvers/${solver}/${solver}-train -l ${l} -k ${k} -t ${t} -r ${r} -s ${s} app.trva.ffm kaggle-avazu/base/${experiment_name}/tr.r0.app.sp.${solver}${suffix}.model 2>&1 | tee output/${experiment_name}/avazu.app.${solver}${suffix}.log
    t=$(../../util/best.py ${2} | awk '{print $2}')
    echo "training site, t = ${t}"
    ../../solvers/${solver}/${solver}-train -l ${l} -k ${k} -t ${t} -r ${r} -s ${s} site.trva.ffm kaggle-avazu/base/${experiment_name}/tr.r0.site.sp.${solver}${suffix}.model 2>&1 | tee output/${experiment_name}/avazu.site.${solver}${suffix}.log
    echo "predicting app"
    ../../solvers/${solver}/${solver}-predict app.te.ffm kaggle-avazu/base/${experiment_name}/tr.r0.app.sp.${solver}${suffix}.model kaggle-avazu/base/${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd.no.id
    echo "predicting site"
    ../../solvers/${solver}/${solver}-predict site.te.ffm kaggle-avazu/base/${experiment_name}/tr.r0.site.sp.${solver}${suffix}.model kaggle-avazu/base/${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd.no.id
    echo "adding ids to app predictions"
    ./add_id_to_prd.py kaggle-avazu/base/va.r0.app.sp kaggle-avazu/base/${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd.no.id kaggle-avazu/base/${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd
    echo "adding ids to site predictions"
    ./add_id_to_prd.py kaggle-avazu/base/va.r0.site.sp kaggle-avazu/base/${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd.no.id kaggle-avazu/base/${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd
    echo "merging predictions"
    cd kaggle-avazu/base/
    ./util/pickle_prediction.py ${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd ${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd.pickle
    ./util/pickle_prediction.py ${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd ${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd.pickle
    ../../merge_prediction.py ${experiment_name}/va.r0.app.sp.${solver}${suffix}.prd.pickle ${experiment_name}/va.r0.site.sp.${solver}${suffix}.prd.pickle ${experiment_name}/va.r0.prd.pickle
    ./util/unpickle_prediction.py ${experiment_name}/va.r0.prd.pickle ${experiment_name}/base.r0.prd.${solver}${suffix}
    echo "Removing model files and intermediate prediction files"
    rm ${experiment_name}/*model ${experiment_name}/va.r0.*
    cd ../..
    echo 'Computing training time'
    train_time_app=$(tail output/${experiment_name}/avazu.app.${solver}${suffix}.log -n 1 | awk '{print $3}')
    train_time_site=$(tail output/${experiment_name}/avazu.site.${solver}${suffix}.log -n 1 | awk '{print $3}')
    echo "scale=0; ($train_time_app + $train_time_site)/1" | bc -l > output/${experiment_name}/${solver}${suffix}.time
    submit_to_kaggle
}

ensemble () {
    cd kaggle-avazu/base/
    echo 'ensembling'
    ./util/pickle_prediction.py ${experiment_name}/base.r0.prd.$1 ${experiment_name}/base.r0.prd.$1.pkl
    ./util/pickle_prediction.py ${experiment_name}/base.r0.prd.$2 ${experiment_name}/base.r0.prd.$2.pkl
    ../../merge_prediction.py ${experiment_name}/base.r0.prd.$1.pkl ${experiment_name}/base.r0.prd.$2.pkl ${experiment_name}/base.r0.prd.$3.pkl
    ./util/unpickle_prediction.py ${experiment_name}/base.r0.prd.$3.pkl ${experiment_name}/base.r0.prd.$3
    echo "removing intermediate pkl prediction files"
    rm ${experiment_name}/*pkl
    cd ../..
    solver=$3
    suffix=''
    submit_to_kaggle
}

symlink_utils
generate_dataset ##1h40m

experiment_name='paper'
solver=linear
r=0.2 l=0 k=4 t=10 suffix=''
train

solver=poly2
r=0.2 l=0 k=4 t=10 suffix=''
train

solver=fm
r=0.05 l=2e-5 k=40 t=8 suffix=''
train

solver=fm_neutral
r=0.05 l=2e-5 k=40 t=8 suffix=''
train

solver=fm_reversed
r=0.05 l=2e-5 k=40 t=8 suffix=''
train

ensemble fm fm_reversed fm_AND_fm_reversed

solver=fm
r=0.05 l=2e-5 k=100 t=9 suffix='2'
train

solver=fm_neutral
r=0.05 l=2e-5 k=100 t=9 suffix='2'
train

solver=fm_reversed
r=0.05 l=2e-5 k=100 t=9 suffix='2'
train

ensemble fm2 fm_reversed2 fm2_AND_fm_reversed2

solver=ffm
r=0.2 l=2e-5 k=4 t=4 suffix=''
train

solver=ffm_reversed
r=0.2 l=2e-5 k=4 t=4 suffix=''
train

ensemble ffm ffm_reversed ffm_AND_ffm_reversed

ensemble fm linear fm_AND_linear

ensemble fm poly2 fm_AND_poly2

ensemble poly2 linear poly2_AND_linear

ensemble fm_reversed linear fm_reversed_AND_linear

ensemble fm_reversed poly2 fm_reversed_AND_poly2

ensemble fm_neutral linear fm_neutral_AND_linear

ensemble fm_neutral poly2 fm_neutral_AND_poly2

experiment_name='fm_neutralk80_auto_stop_chrono'
mkdir output/${experiment_name}
solver=fm_neutral
r=0.05 l=2e-5 k=80 t=10 suffix='k80'
for what in app site
do
    ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p kaggle-avazu/base/trva_chrono.r0.${what}.sp.ffm kaggle-avazu/base/trtr_chrono.r0.${what}.sp.ffm avazu.model 2>&1 | tee output/${experiment_name}/avazu.auto_stop.${solver}${suffix}.${what}.log
done

experiment_name='paper'
mkdir output/${experiment_name}
solver=fm_neutral
r=0.05 l=2e-5 k=80 suffix='k80_auto_stop_chrono'
train_vary_t output/fm_neutralk80_auto_stop_chrono/avazu.auto_stop.fm_neutralk80.app.log output/fm_neutralk80_auto_stop_chrono/avazu.auto_stop.fm_neutralk80.site.log


experiment_name='fm_neutralk80_auto_stop_chrono_grid_search'
mkdir output/${experiment_name}
solver=fm_neutral
k=80 t=20
for r in 0.02 0.05 0.1
do
    for l in 2e-4 2e-5 2e-6
    do
        for what in app site
        do
            suffix='k80'_r${r}_l${l}
            echo "../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p kaggle-avazu/base/trva_chrono.r0.${what}.sp.ffm kaggle-avazu/base/trtr_chrono.r0.${what}.sp.ffm avazu.model 2>&1 | tee output/${experiment_name}/avazu.auto_stop.${solver}${suffix}.${what}.log"
            ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p kaggle-avazu/base/trva_chrono.r0.${what}.sp.ffm kaggle-avazu/base/trtr_chrono.r0.${what}.sp.ffm avazu.model 2>&1 | tee output/${experiment_name}/avazu.auto_stop.${solver}${suffix}.${what}.log
        done
    done
done

experiment_name='fm_neutralk80_auto_stop_chrono_results'
mkdir output/${experiment_name}
solver=fm_neutral
k=80 t=1000
for r in 0.02 0.05 0.1
do
    for l in 2e-4 2e-5 2e-6
    do
        suffix='k80'_r${r}_l${l}
        train_vary_t output/fm_neutralk80_auto_stop_chrono_grid_search/avazu.auto_stop.fm_neutral${suffix}.app.log output/fm_neutralk80_auto_stop_chrono_grid_search/avazu.auto_stop.fm_neutral${suffix}.site.log
    done
done


experiment_name='IO_fm_paper_auto_stop_chrono'
mkdir output/${experiment_name}
r=0.05 l=2e-5 k=40 t=20 suffix=''
for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
do
    for symmetry in "" "_neutral" "_reversed"
    do
        for what in app site
        do
            solver=fm${order}${symmetry}
            ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p kaggle-avazu/base/trva_chrono.r0.${what}.sp.ffm kaggle-avazu/base/trtr_chrono.r0.${what}.sp.ffm avazu.model 2>&1 | tee output/${experiment_name}/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.${what}.log
            best_epoch=$(../../util/best.py output/${experiment_name}/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.${what}.log)
            echo ${best_epoch} > output/${experiment_name}/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.${what}.best_epoch
        done
    done
done

experiment_name='IO_fm_paper_vary_t'
mkdir output/${experiment_name}
r=0.05 l=2e-5 k=40 t=none suffix=''
for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
do
    for symmetry in "" "_neutral" "_reversed"
    do
        solver=fm${order}${symmetry}
        train_vary_t output/IO_fm_paper_auto_stop_chrono/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.app.log output/IO_fm_paper_auto_stop_chrono/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.site.log
    done
    ensemble "fm${order}" "fm${order}_reversed" "fm${order}_AND_fm${order}_reversed"
done

experiment_name='IO_fm_w_linear_paper_auto_stop_chrono'
mkdir output/${experiment_name}
r=0.05 l=2e-5 k=40 t=20 suffix=''
for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
do
    for symmetry in "" "_neutral" "_reversed"
    do
        for what in app site
        do
            solver=fm${order}_w_linear${symmetry}
            ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p kaggle-avazu/base/trva_chrono.r0.${what}.sp.ffm kaggle-avazu/base/trtr_chrono.r0.${what}.sp.ffm avazu.model 2>&1 | tee output/${experiment_name}/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.${what}.log
            best_epoch=$(../../util/best.py output/${experiment_name}/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.${what}.log)
            echo ${best_epoch} > output/${experiment_name}/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.${what}.best_epoch
        done
    done
done

experiment_name='IO_fm_w_linear_paper_vary_t'
mkdir output/${experiment_name}
r=0.05 l=2e-5 k=40 t=none suffix=''
for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
do
    for symmetry in "" "_neutral" "_reversed"
    do
        solver=fm${order}_w_linear${symmetry}
        train_vary_t output/IO_fm_w_linear_paper_auto_stop_chrono/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.app.log output/IO_fm_w_linear_paper_auto_stop_chrono/avazu.auto_stop.${solver}${suffix}_l${l}_r${r}.site.log
    done
    ensemble "fm${order}_w_linear" "fm${order}_w_linear_reversed" "fm${order}_w_linear_AND_fm${order}_w_linear_reversed"
done

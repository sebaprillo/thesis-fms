#!/usr/bin/env python

import sys
import pandas as pd

src = sys.argv[1]
tgt = sys.argv[2]
sample = sys.argv[3]

df = pd.read_csv(src, index_col = ['id'], dtype={'id':str})
df_sample = pd.read_csv(sample, index_col= ['id'], dtype={'id':str})
df_sorted = df.loc[df_sample.index]
df_sorted.to_csv(tgt)

#!/bin/bash

s=8

dataset_name=criteo
mkdir output
kaggle competitions leaderboard criteo-display-ad-challenge -d
rm criteo-display-ad-challenge-publicleaderboard.csv
unzip criteo-display-ad-challenge.zip

symlink_utils () {
    echo "Symlinking utils"
    ln -sf ../../util/get_standing.py
}

generate_dataset () {
    #git clone https://github.com/guestwalk/kaggle-2014-criteo.git #No longer clone, added it to repo
    cd kaggle-2014-criteo
    make
    cd ..
    
    tar -xzf ../../data/criteo/dac.tar.gz
    
    ./kaggle-2014-criteo/converters/txt2csv.py tr train.txt train.csv
    ./kaggle-2014-criteo/converters/txt2csv.py te test.txt test_without_label.csv
    ./kaggle-2014-criteo/utils/add_dummy_label.py test_without_label.csv test.csv
    
    ln -sf train.csv tr.csv
    ln -sf test.csv te.csv
    
    ./kaggle-2014-criteo/utils/count.py tr.csv > fc.trva.t10.txt
    
    ./kaggle-2014-criteo/converters/parallelizer-a.py -s 8 ./kaggle-2014-criteo/converters/pre-a.py tr.csv tr.gbdt.dense tr.gbdt.sparse
    ./kaggle-2014-criteo/converters/parallelizer-a.py -s 8 ./kaggle-2014-criteo/converters/pre-a.py te.csv te.gbdt.dense te.gbdt.sparse
    
    ./kaggle-2014-criteo/gbdt -t 0 -s 8 te.gbdt.dense te.gbdt.sparse tr.gbdt.dense tr.gbdt.sparse te.gbdt.out tr.gbdt.out
    
    ./kaggle-2014-criteo/converters/parallelizer-b.py -s 8 ./kaggle-2014-criteo/converters/pre-b.py tr.csv tr.gbdt.out tr.ffm
    
    ./kaggle-2014-criteo/converters/parallelizer-b.py -s 8 ./kaggle-2014-criteo/converters/pre-b.py te.csv te.gbdt.out te.ffm

    ln -sf ./tr.ffm ./criteo.tr
    ln -sf ./te.ffm ./criteo.va
    
    ../../util/split_sequential.py 0.2 criteo.tr criteo_chrono.trtr criteo_chrono.trva 45840617
}

submit_to_kaggle () {
    echo 'Submitting to Kaggle'
    date=$(date)
    kaggle competitions submit -c criteo-display-ad-challenge -f ${experiment_name}/criteo.${solver}${suffix}.output.sub -m "Submit ${experiment_name}/${solver}${suffix} date=${date}"
    while true; do
        echo "Waiting for submission to be scored..."
        sleep 30
        kaggle competitions submissions --csv -c criteo-display-ad-challenge | grep criteo > kaggle_submissions
        head -n 1 kaggle_submissions > ${experiment_name}/criteo.${solver}${suffix}.kaggle_submission
        status=$(awk -F "\"*,\"*" '{print $4}' ${experiment_name}/criteo.${solver}${suffix}.kaggle_submission)
        message=$(awk -F "\"*,\"*" '{print $3}' ${experiment_name}/criteo.${solver}${suffix}.kaggle_submission)
        if [ "$status" != "pending" ] && [ "$message" == "Submit ${experiment_name}/${solver}${suffix} date=${date}" ]; then
            echo "done!"
            break
        fi
    done
    awk -F "\"*,\"*" '{printf("%.5f\n", $5)}' ${experiment_name}/criteo.${solver}${suffix}.kaggle_submission > output/${experiment_name}/${solver}${suffix}.public
    awk -F "\"*,\"*" '{printf("%.5f\n", $6)}' ${experiment_name}/criteo.${solver}${suffix}.kaggle_submission > output/${experiment_name}/${solver}${suffix}.private
    echo 'Computing public standing'
    ./get_standing.py criteo-display-ad-challenge-publicleaderboard.csv output/${experiment_name}/${solver}${suffix}.public output/${experiment_name}/${solver}${suffix}.public_standing
}

train () {
    mkdir output/${experiment_name}
    mkdir ${experiment_name}
    echo "Training: ${solver} -l ${l} -k ${k} -t ${t} -r ${r} -s ${s}, suffix=${suffix}, experiment_name=${experiment_name}"
    ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} criteo.tr ${experiment_name}/criteo.${solver}${suffix}.model 2>&1 | tee output/${experiment_name}/criteo.${solver}${suffix}.log
    echo "Predicting"
    ../../solvers/${solver}/${solver}-predict criteo.va ${experiment_name}/criteo.${solver}${suffix}.model ${experiment_name}/criteo.${solver}${suffix}.output
    echo "Adding id to prediction"
    ./script/add_id_to_prediction.py test.csv ${experiment_name}/criteo.${solver}${suffix}.output ${experiment_name}/criteo.${solver}${suffix}.output.sub
    echo "Removing model files and intermediate prediction files"
    rm ${experiment_name}/*model ${experiment_name}/*output
    echo 'Computing training time'
    train_time_app=$(tail output/${experiment_name}/criteo.${solver}${suffix}.log -n 1 | awk '{print $3}')
    train_time_site=$(tail output/${experiment_name}/criteo.${solver}${suffix}.log -n 1 | awk '{print $3}')
    echo "scale=0; ($train_time_app + $train_time_site)/1" | bc -l > output/${experiment_name}/${solver}${suffix}.time
    submit_to_kaggle
}

ensemble () {
    echo 'ensembling'
    ./merge_prediction.py ${experiment_name}/criteo.${1}.output.sub ${experiment_name}/criteo.${2}.output.sub ${experiment_name}/criteo.${3}.output.sub.pkl
    ./unpickle_prediction.py ${experiment_name}/criteo.${3}.output.sub.pkl ${experiment_name}/criteo.${3}.output.sub
    echo "removing intermediate pkl prediction files"
    rm ${experiment_name}/*pkl
    solver=$3
    suffix=''
    submit_to_kaggle
}

symlink_utils
generate_dataset

experiment_name='paper'
solver=linear
r=0.2 l=0 k=4 t=13 suffix=''
train

solver=poly2
r=0.2 l=0 k=4 t=10 suffix=''
train

solver=fm
r=0.05 l=0.00002 k=40 t=8 suffix=''
train

solver=fm_neutral
r=0.05 l=0.00002 k=40 t=8 suffix=''
train

solver=fm_reversed
r=0.05 l=0.00002 k=40 t=8 suffix=''
train

ensemble fm fm_reversed fm_AND_fm_reversed

solver=fm
r=0.05 l=0.00002 k=100 t=9 suffix='2'
train

solver=fm_neutral
r=0.05 l=0.00002 k=100 t=9 suffix='2'
train

solver=fm_reversed
r=0.05 l=0.00002 k=100 t=9 suffix='2'
train

ensemble fm2 fm_reversed2 fm2_AND_fm_reversed2

solver=ffm
r=0.2 l=0.00002 k=4 t=9 suffix=''
train

solver=ffm_reversed
r=0.2 l=0.00002 k=4 t=9 suffix=''
train

ensemble ffm ffm_reversed ffm_AND_ffm_reversed

ensemble fm2 linear fm2_AND_linear

ensemble fm2 poly2 fm2_AND_poly2

ensemble poly2 linear poly2_AND_linear

ensemble fm_reversed2 linear fm_reversed2_AND_linear

ensemble fm_reversed2 poly2 fm_reversed2_AND_poly2

ensemble fm_neutral2 linear fm_neutral2_AND_linear

ensemble fm_neutral2 poly2 fm_neutral2_AND_poly2

experiment_name='fm2k200_auto_stop'
mkdir output/${experiment_name}
solver=fm_neutral
r=0.05 l=0.00002 k=200 t=10 suffix='2k200'
time ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p criteo_chrono.trva criteo_chrono.trtr criteo.model 2>&1 | tee output/${experiment_name}/criteo.auto_stop.${solver}${suffix}.log

experiment_name='paper'
mkdir output/${experiment_name}
solver=fm_neutral
r=0.05 l=0.00002 k=200 suffix='2k200_auto_stop'
t=$(../../util/best.py output/fm2k200_auto_stop/criteo.auto_stop.fm_neutral2k200.log | awk '{print $2}')
train

experiment_name='fm2k200_grid_search'
mkdir output/${experiment_name}
solver=fm_neutral
for r in 0.02 0.05 0.1
do
    for l in 2e-4 2e-5 2e-6
    do
        k=200 t=20 suffix="2k200_r${r}_l${l}"
        time ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p criteo_chrono.trva criteo_chrono.trtr criteo.model 2>&1 | tee output/${experiment_name}/criteo.auto_stop.${solver}${suffix}.log
        t=$(../../util/best.py output/${experiment_name}/criteo.auto_stop.${solver}${suffix}.log | awk '{print $2}')
        train
    done
done

experiment_name='IO_fm_paper_auto_stop_chrono'
mkdir output/${experiment_name}
for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
do
    for symmetry in "" "_neutral" "_reversed"
    do
        solver="fm${order}${symmetry}"
        r=0.05 l=0.00002 k=100 t=10 suffix='2'
        time ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p criteo_chrono.trva criteo_chrono.trtr criteo.model 2>&1 | tee output/${experiment_name}/criteo.auto_stop.${solver}${suffix}.log
    done
done

experiment_name='IO_fm_paper_chrono_vary_t'
for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
do
    for symmetry in "" "_neutral" "_reversed"
    do
        solver="fm${order}${symmetry}"
        r=0.05 l=0.00002 k=100 suffix='2'
        t=$(../../util/best.py output/IO_fm_paper_auto_stop_chrono/criteo.auto_stop.${solver}${suffix}.log | awk '{print $2}')
        echo "${solver}, t = ${t}"
        train
    done
    ensemble "fm${order}${suffix}" "fm${order}_reversed${suffix}" "fm${order}${suffix}_AND_fm${order}_reversed${suffix}"
done


experiment_name='IO_fm_w_linear_paper_auto_stop_chrono'
mkdir output/${experiment_name}
for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
do
    for symmetry in "" "_neutral" "_reversed"
    do
        solver="fm${order}_w_linear${symmetry}"
        r=0.05 l=0.00002 k=100 t=10 suffix='2'
        time ../../solvers/${solver}/${solver}-train -l ${l} -t ${t} -r ${r} -s ${s} -k ${k} -p criteo_chrono.trva criteo_chrono.trtr criteo.model 2>&1 | tee output/${experiment_name}/criteo.auto_stop.${solver}${suffix}.log
    done
done

experiment_name='IO_fm_w_linear_paper_chrono_vary_t'
for order in 1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0
do
    for symmetry in "" "_neutral" "_reversed"
    do
        solver="fm${order}_w_linear${symmetry}"
        r=0.05 l=0.00002 k=100 suffix='2'
        t=$(../../util/best.py output/IO_fm_w_linear_paper_auto_stop_chrono/criteo.auto_stop.${solver}${suffix}.log | awk '{print $2}')
        echo "${solver}, t = ${t}"
        train
    done
    ensemble "fm${order}_w_linear${suffix}" "fm${order}_w_linear_reversed${suffix}" "fm${order}_w_linear${suffix}_AND_fm${order}_w_linear_reversed${suffix}"
done


# coding: utf-8

# In[ ]:

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import seaborn as sns


# In[ ]:

model_names = [
    'linear',
    'poly2',
    #'fm',
    #'fm_neutral',
    #'fm_reversed',
    #'fm_AND_fm_reversed',
    'fm2',
    'fm_neutral2',
    'fm_reversed2',
    'fm2_AND_fm_reversed2',
    'ffm',
    'ffm_reversed',
    'ffm_AND_ffm_reversed',
    'fm2_AND_poly2',
    'fm2_AND_linear',
    'poly2_AND_linear'
]


# In[ ]:

dfs = []
for model_name in model_names:
    df = pd.read_csv('paper/criteo.' + model_name + '.output.sub', index_col=['Id'], names=['Id', model_name], skiprows=1)
    df.colnames = [model_name]
    df[model_name] = np.log(df[model_name] / (1.0 - df[model_name]))
    dfs.append(df)


# In[ ]:

df = pd.concat(dfs, axis=1)


# In[ ]:

corrmat = df[["fm2", 
              "fm_reversed2", 
              "fm_neutral2", 
              "ffm", 
              "ffm_reversed", 
              "poly2", 
              "linear"]].corr()
names = ["FM-Standard",
        "FM-Reversed",
        "FM-Neutral",
        "FFM",
        "FFM-Reversed",
        "Poly2",
        "LM"]
_, ax = plt.subplots(figsize=(14,12))
sns.set(font_scale=1.0)
correlation_heatmap = sns.heatmap(corrmat, square=True, annot=True, fmt='.3f', annot_kws={'size': 14},
                                 xticklabels = names, yticklabels = names)
plt.yticks(rotation=0)
correlation_heatmap.get_figure().savefig("output/correlation_heatmap.png")
plt.show()


# In[ ]:

from numpy.linalg import svd
columns = ["poly2",
           #"fm",
           #"fm_neutral",
           #"fm_reversed",
           #"fm_AND_fm_reversed",
           "fm2",
           "fm_neutral2",
           "fm_reversed2",
           "fm2_AND_fm_reversed2",
           "ffm",
           "ffm_reversed",
           "ffm_AND_ffm_reversed"]
u, s, vh = np.linalg.svd(df[columns].corr(), full_matrices=True)
k = 2
#np.matmul(u[:, :k], np.matmul(np.diag(s[:k]), vh[:k, :]))
embeddings = u[:, :k]


# In[ ]:

# De aca: https://stackoverflow.com/questions/5147112/how-to-put-individual-tags-for-a-scatter-plot
plt.subplots_adjust(bottom = 0.1)
plt.figure(figsize=(15,10))
plt.scatter(
    embeddings[:, 0], embeddings[:, 1], marker='o',
    cmap=plt.get_cmap('Spectral'))

for label, x, y in zip(columns, embeddings[:, 0], embeddings[:, 1]):
    text_y_offset = 20
    text_x_offset = -20
    plt.annotate(
        label,
        xy=(x, y), xytext=(text_x_offset, text_y_offset),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
plt.savefig("output/models_map.png")
plt.show()


# In[ ]:

def my_jointplot(solver1, solver2, solver1_name, solver2_name, name, kind="hex", sz=len(df)):
    df_subset = df.sample(sz)
    jointplot = sns.jointplot(df_subset[[solver1]], df_subset[[solver2]], kind=kind, color="#4CB391")
    x0, x1 = jointplot.ax_joint.get_xlim()
    y0, y1 = jointplot.ax_joint.get_ylim()
    lims = [max(x0, y0), min(x1, y1)]
    jointplot.ax_joint.plot(lims, lims, ':k')
    jointplot.set_axis_labels(solver1_name, solver2_name)
    jointplot.savefig("output/%s"%name)
    plt.show()
my_jointplot('fm2', 'fm_reversed2', 'FM-Standard', 'FM-Reversed', 'fm2_AND_fm_reversed2.png', kind="kde", sz=10000)
my_jointplot('ffm', 'ffm_reversed', 'FFM', 'FFM-Reversed', 'ffm_AND_ffm_reversed.png', kind="kde", sz=10000)


# In[ ]:

def plot_unity(xdata, ydata, **kwargs):
    mn = min(xdata.min(), ydata.min())
    mx = max(xdata.max(), ydata.max())
    points = np.linspace(mn, mx, 100)
    plt.gca().plot(points, points, color='r', marker=None,
            linestyle='-', linewidth=0.5)

model_pairplot = sns.pairplot(df[["fm2", "fm_reversed2", "fm_neutral2", "ffm", "ffm_reversed", "poly2", "linear"]].sample(10000, random_state=0),
                             plot_kws={'alpha': 0.03})
model_pairplot.map_offdiag(plot_unity)


# In[ ]:

model_pairplot.savefig("output/models_pairplot.png")


# In[ ]:

def plot_io_and_symmetry(dataset, grid_search_results_dir, what, leaderboard='', w_linear='_w_linear', name='IO', suffix=''):
    '''
    :param what: '' or 'no_diag'
    '''
    assert what in ['', '_no_diag']
    vmin = vmins[dataset]
    vmax = vmaxs[dataset]
    def get_loss(dataset, grid_search_results_dir, solver):
        import os
        loss_file_path = '{grid_search_results_dir}/{solver}{suffix}.{leaderboard}'.format(suffix=suffix, dataset=dataset, grid_search_results_dir=grid_search_results_dir, solver=solver, leaderboard=leaderboard)
        if not os.path.isfile(loss_file_path):
            print("file %s not found" % loss_file_path)
        assert os.path.isfile(loss_file_path)
        res = 0.0
        for line in open(loss_file_path):
            res = float(line)
        return res
    symmetries = [
        '',
        '_neutral',
        'ensemble',
        '_reversed'
    ]
    symmetry_names = [
        'FM-Standard',
        'FM-Neutral',
        'FM-Standard + FM-Reversed',
        'FM-Reversed'
    ]
    symmetry_idx = [0, 1, 3]
    ensemble_idx = 2
    rows_to_show = [0, 1, 2, 3]
    assert(ensemble_idx not in symmetry_idx)
    orders = [
        'fm1p25',
        'fm1p5',
        'fm1p75',
        'fm2p0',
        'fm2p25',
        'fm2p5',
        'fm2p75',
        'fm3p0',
    ]
    order_names = [
        '1.25',
        '1.5',
        '1.75',
        '2.0',
        '2.25',
        '2.5',
        '2.75',
        '3.0',
    ]
    num_symmetries = len(symmetries)
    num_orders = len(orders)
    loss = np.zeros((num_symmetries, num_orders))
    for i in symmetry_idx:
        symmetry = symmetries[i]
        for j, order in enumerate(orders):
            solver = '{order}{w_linear}{symmetry}{what}'.format(w_linear=w_linear, order=order, symmetry=symmetry, what=what)
            loss[i][j] = get_loss(dataset, grid_search_results_dir, solver=solver)
            loss[ensemble_idx][j] = get_loss(dataset, grid_search_results_dir, solver='{order}{w_linear}{what}{suffix}_AND_{order}{w_linear}_reversed{what}'.format(suffix=suffix, w_linear=w_linear, order=order, what=what))
    plt.figure(figsize=(12, 4))
    ax = sns.heatmap(loss[rows_to_show], 
                     linewidth=0.5, 
                     vmin=vmin, 
                     vmax=vmax,
                     annot=True,
                     fmt='.5f',
                     square=True,
                     xticklabels=order_names,
                     yticklabels=np.array(symmetry_names)[rows_to_show],
                     cmap = 'Greys')
    plt.title(dataset)
    plt.yticks(rotation=0)
    plt.xlabel('interaction order')
    plt.ylabel('symmetry type')
    plt.savefig("output/{name}_{leaderboard}".format(leaderboard=leaderboard,name=name))
    plt.show()


# In[ ]:

vmins = {'criteo' : 0.4465}
vmaxs = {'criteo' : 0.4490}
plot_io_and_symmetry(dataset='criteo', 
                     grid_search_results_dir='output/IO_fm_paper_chrono_vary_t', 
                     what='',
                     leaderboard='public',
                     w_linear='',
                     name='IO_fm_paper_chrono_vary_t',
                     suffix='2')


# In[ ]:

vmins = {'criteo' : 0.4465}
vmaxs = {'criteo' : 0.4490}
plot_io_and_symmetry(dataset='criteo', 
                     grid_search_results_dir='output/IO_fm_w_linear_paper_chrono_vary_t', 
                     what='',
                     leaderboard='public',
                     w_linear='_w_linear',
                     name='IO_fm_w_linear_paper_chrono_vary_t',
                     suffix='2')


# In[ ]:




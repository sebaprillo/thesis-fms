
# coding: utf-8

# In[ ]:

import numpy as np
import pandas as pd
def get_loss(dataset, grid_search_results_dir, solver):
        import os
        loss_file_path = 'moredata/{dataset}/{grid_search_results_dir}/{solver}.loss'.format(dataset=dataset, grid_search_results_dir=grid_search_results_dir, solver=solver)
        if not os.path.isfile(loss_file_path):
            print("file %s not found" %loss_file_path)
        assert os.path.isfile(loss_file_path)
        res = 0.0
        for line in open(loss_file_path):
            res = float(line)
        return res


# In[ ]:

def get_dataset_improvement_by_ensembling(dataset, grid_search_results_dir = 'grid_search_results'):
    import os
    orders = [
            'fm1p25',
            'fm1p5',
            'fm1p75',
            'fm2p0',
            'fm2p25',
            'fm2p5',
            'fm2p75',
            'fm3p0',
            #'fmho3',
            #'fmho2and3'
        ]
    symmetries = [
            '',
            'ensemble',
            '_reversed'
        ]
    losses = np.zeros(shape=(len(orders), len(symmetries)))
    for i, order in enumerate(orders):
        for j, symmetry in enumerate(symmetries):
            if symmetry == 'ensemble':
                solver = order + '_w_linear_AND_' + order + '_w_linear_reversed'
            else:
                solver = order + '_w_linear' + symmetry
            loss = get_loss(dataset, grid_search_results_dir, solver)
            losses[i, j] = loss
    print(losses)
    improvements = np.min(losses[:, [0, 2]], axis=1) - losses[:, 1]
    print(improvements)
    print(np.mean(improvements))
    write_path = 'moredata/{dataset}/{grid_search_results_dir}/ensemble_improvement.tex'.format(dataset=dataset, grid_search_results_dir=grid_search_results_dir)
    with open(write_path, 'w+') as f_out:
        f_out.write("%.4f"%np.mean(improvements))
    write_path = 'moredata/{dataset}/{grid_search_results_dir}/ensemble_improvement_2p0.tex'.format(dataset=dataset, grid_search_results_dir=grid_search_results_dir)
    with open(write_path, 'w+') as f_out:
        f_out.write("%.4f"%np.mean(np.min(losses[3, [0, 2]]) - losses[3, 1]))
    return np.mean(improvements)


# In[ ]:

get_dataset_improvement_by_ensembling('movielens-100K')


# In[ ]:

get_dataset_improvement_by_ensembling('movielens-100K-all')


# In[ ]:

def get_dataset_improvement_by_ensembling_ffm(dataset, grid_search_results_dir = 'grid_search_results'):
    import os
    orders = [
            'ffm',
        ]
    symmetries = [
            '',
            'ensemble',
            '_reversed'
        ]
    losses = np.zeros(shape=(len(orders), len(symmetries)))
    for i, order in enumerate(orders):
        for j, symmetry in enumerate(symmetries):
            if symmetry == 'ensemble':
                solver = order + '_w_linear_AND_' + order + '_w_linear_reversed'
            else:
                solver = order + '_w_linear' + symmetry
            loss = get_loss(dataset, grid_search_results_dir, solver)
            losses[i, j] = loss
    print(losses)
    improvements = np.min(losses[:, [0, 2]], axis=1) - losses[:, 1]
    print(improvements)
    print(np.mean(improvements))
    write_path = 'moredata/{dataset}/{grid_search_results_dir}/ensemble_improvement_ffm.tex'.format(dataset=dataset, grid_search_results_dir=grid_search_results_dir)
    with open(write_path, 'w+') as f_out:
        f_out.write("%.4f"%np.mean(improvements))
    return np.mean(improvements)


# In[ ]:

get_dataset_improvement_by_ensembling_ffm('movielens-100K')


# In[ ]:

get_dataset_improvement_by_ensembling_ffm('movielens-100K-all')


# In[ ]:




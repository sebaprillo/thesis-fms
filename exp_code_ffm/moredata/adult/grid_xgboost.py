
# coding: utf-8

# In[ ]:

import xgboost as xgb
import pandas as pd
import numpy as np
from sklearn import preprocessing


# In[ ]:

import os
if not os.path.isdir('grid_search_results'):
    os.mkdir('grid_search_results')
path_train = '../../data/adult/adult.data'
path_test = '../../data/adult/adult.test'
path_loss = 'grid_search_results/xgboost.loss'
path_cmd = 'grid_search_results/xgboost.cmd'
skip_rows_test=1
n_splits=5
objective = 'binary:logistic'
eval_metric = 'logloss'
subset_y_at_position = 1
learning_rates = [0.1,0.3,1.0]
max_depths = [3,5,7,10]
reg_lambdas = [0,1,3,10]
n_estimators = 1000
early_stopping_rounds = 100
def calc_loss(pred_te, y_te):
    def normalize(v):
        return np.array([min(max(x,1e-5),1-1e-5) for x in v])
    pred_te = normalize(pred_te)
    y_te = normalize(y_te)
    return -np.mean(np.multiply(y_te, np.log(pred_te)) + (1.0 - y_te) * np.log(1.0 - pred_te))


# In[ ]:

df_trva = pd.read_table(path_train, sep=',', header=None)
df_te = pd.read_table(path_test,  sep=',', header=None, skiprows=skip_rows_test)
df_trva.iloc[:,-1] = df_trva.iloc[:,-1].apply(lambda x : x[subset_y_at_position])
df_te.iloc[:,-1] = df_te.iloc[:,-1].apply(lambda x : x[subset_y_at_position])
len_trva = len(df_trva)
len_te = len(df_te)
df_trvate = pd.concat([df_trva, df_te], axis=0)
for i, c in enumerate(df_trvate.columns):
    if df_trvate.dtypes[i] == 'object':
        print('Converting %s to str' %c)
        lb = preprocessing.LabelEncoder()
        lb.fit(df_trvate[c].astype('str'))
        df_trvate[c] = lb.transform(df_trvate[c].astype('str'))
df_trva = df_trvate.iloc[range(len_trva),:]
df_te = df_trvate.iloc[range(len_trva, len_trva + len_te),:]
y_trva = df_trva.iloc[:,-1]
y_te = df_te.iloc[:,-1]
df_trva = df_trva.iloc[:,range(df_trva.shape[1] - 1)]
df_te = df_te.iloc[:,range(df_te.shape[1] - 1)]


# In[ ]:

results = []
for learning_rate in learning_rates:
    for max_depth in max_depths:
        for reg_lambda in reg_lambdas:
            params = {
                'objective':objective, 
                'learning_rate':learning_rate,
                'max_depth':max_depth, 
                'reg_lambda':reg_lambda,
                'n_estimators':n_estimators,
            }
            model = xgb.XGBRegressor(**params)
            from sklearn.model_selection import KFold
            kfold = KFold(n_splits=n_splits, shuffle=True, random_state=1)
            for i, (int_idx_tr, int_idx_va) in enumerate(kfold.split(df_trva, y_trva)):
                if i > 0:
                    break
                idx_tr = df_trva.iloc[int_idx_tr].index
                idx_va = df_trva.iloc[int_idx_va].index
                eval_set = [(df_trva.loc[idx_va], y_trva[idx_va])]
                model.fit(df_trva.loc[idx_tr], y_trva[idx_tr], verbose=False, early_stopping_rounds = early_stopping_rounds, eval_set = eval_set, eval_metric=eval_metric)
            results.append([params, model.best_score, model.best_iteration])
best_params = None
best_loss = None
best_iter = None
for params, loss, iter in results:
    if best_loss is None or best_loss > loss:
        best_params = params
        best_loss = loss
        best_iter = iter
print('Best params/loss/iteration found:')
[best_params, best_loss, best_iter]


# In[ ]:

params = best_params
params['n_estimators'] = best_iter
model = xgb.XGBRegressor(**params)
model.fit(df_trva, y_trva, verbose=True, eval_set = [(df_te,y_te)], eval_metric=eval_metric)
pred_te = model.predict(data=df_te)
loss = calc_loss(pred_te, y_te)


# In[ ]:

with open(path_loss, 'w+') as f_out:
    f_out.write("%.5f"%loss)
with open(path_cmd, 'w+') as f_out:
    f_out.write(str([best_params, best_loss, best_iter]))


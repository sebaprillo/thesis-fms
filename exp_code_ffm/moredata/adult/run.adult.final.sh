#!/bin/bash

n_thread=36
s=1

#set -e

source ../../util/common.sh

counter=0
dataset_name=adult

mkdir grid_search_results
mkdir grid_search_results/best_params

cd ../../
make
cd moredata/${dataset_name}

generate_dataset () {
    echo "Generating adult dataset"
    ln -sf ../../util/split.py
    ln -sf ../../data/adult/adult.data
    ln -sf ../../data/adult/adult.test
    sed 1d adult.test> adult.test_
    ./txt2csv.py adult.data adult.tr.csv
    ./txt2csv.py adult.test_ adult.te.csv
    ./cvt.adult.py adult.tr.csv adult.te.csv adult.trva.ffm adult.te.ffm
    ./split.py 0.2 adult.trva.ffm adult.tr.ffm adult.va.ffm
    ln -sf adult.tr.ffm tr.ffm
    ln -sf adult.va.ffm va.ffm
    ln -sf adult.trva.ffm trva.ffm
    ln -sf adult.te.ffm te.ffm
}

grid_linear_and_poly2 () {
    echo "gridding LM and Poly2"
    mkdir grid_search_results
    solvers=(linear poly2_w_linear linear_reversed poly2_w_linear_reversed)
    l_list_baselines=0,1e-6,2e-6,5e-6,1e-5,2e-5,5e-5,1e-4,2e-4,5e-4,1e-3,2e-3,5e-3,1e-2,2e-3,5e-3
    k_list_baselines=4 #unused
    r_list_baselines=0.05,0.1,0.2
    t_baselines=100
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_baselines} ${k_list_baselines} ${r_list_baselines} ${t_baselines} ${s}
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_baselines} ${k_list_baselines} ${r_list_baselines} ${t_baselines} ${s} &
        fi
    done
}

grid_ffms () {
    echo "gridding FFMs"
    mkdir grid_search_results
    solvers=(ffm_w_linear ffm_w_linear_reversed)
    l_list_ffms=0,0.00001,0.0001,0.001
    k_list_ffms=1,2,4,8
    r_list_ffms=0.02,0.05,0.1
    t_ffms=50
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_ffms} ${k_list_ffms} ${r_list_ffms} ${t_ffms} ${s}
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_ffms} ${k_list_ffms} ${r_list_ffms} ${t_ffms} ${s} &
        fi
    done
}

grid_fms () {
    echo "gridding FMs"
    mkdir grid_search_results
    solvers=(fm1p25_w_linear fm1p5_w_linear fm1p75_w_linear fm2p0_w_linear fm2p25_w_linear fm2p5_w_linear fm2p75_w_linear fm3p0_w_linear fm1p25_w_linear_neutral fm1p5_w_linear_neutral fm1p75_w_linear_neutral fm2p0_w_linear_neutral fm2p25_w_linear_neutral fm2p5_w_linear_neutral fm2p75_w_linear_neutral fm3p0_w_linear_neutral fm1p25_w_linear_reversed fm1p5_w_linear_reversed fm1p75_w_linear_reversed fm2p0_w_linear_reversed fm2p25_w_linear_reversed fm2p5_w_linear_reversed fm2p75_w_linear_reversed fm3p0_w_linear_reversed)
    l_list_fms=0,0.0001,0.001,0.01
    k_list_fms=16,32,64,128
    r_list_fms=0.02,0.05,0.1
    t_fms=50
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_fms} ${k_list_fms} ${r_list_fms} ${t_fms} ${s}
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_fms} ${k_list_fms} ${r_list_fms} ${t_fms} ${s} &
        fi
    done
}

grid_xgboost () {
    python grid_xgboost.py
}

symlink_utils
generate_dataset
grid_ffms #2
grid_fms #6+24
grid_linear_and_poly2 #4
grid_xgboost
final_ensembles

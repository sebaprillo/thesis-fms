#!/bin/bash

n_thread=36
s=1

#set -e

source ../../util/common.sh

counter=0
dataset_name=movielens-100K-all

mkdir grid_search_results
mkdir grid_search_results/best_params

cd ../../
make
cd moredata/${dataset_name}

generate_dataset () {
    echo "Generating ${dataset_name} dataset"
    mkdir ml-100k
    python to_ffm.py
    ../../util/split.py 0.2 ml-100k/u.ffm ml-100k/u_trva.ffm ml-100k/u_te.ffm
    ../../util/split.py 0.2 ml-100k/u_trva.ffm ml-100k/u_tr.ffm ml-100k/u_va.ffm
    ln -sf ml-100k/u_tr.ffm tr.ffm
    ln -sf ml-100k/u_va.ffm va.ffm
    ln -sf ml-100k/u_trva.ffm trva.ffm
    ln -sf ml-100k/u_te.ffm te.ffm
    ../../util/ffm2fm_regression.py tr.ffm tr.fm
    ../../util/ffm2fm_regression.py va.ffm va.fm
    ../../util/ffm2fm_regression.py trva.ffm trva.fm
    ../../util/ffm2fm_regression.py te.ffm te.fm
}

grid_linear_and_poly2 () {
    echo "gridding LM and Poly2"
    mkdir grid_search_results
    solvers=(linear poly2_w_linear linear_reversed poly2_w_linear_reversed)
    l_list_baselines=0,1e-6,2e-6,5e-6,1e-5,2e-5,5e-5,1e-4,2e-4,5e-4,1e-3,2e-3,5e-3,1e-2,2e-3,5e-3
    k_list_baselines=4 #unused
    r_list_baselines=0.1,0.2,0.5
    t_baselines=100
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_baselines} ${k_list_baselines} ${r_list_baselines} ${t_baselines} ${s} r
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_baselines} ${k_list_baselines} ${r_list_baselines} ${t_baselines} ${s} r &
        fi
    done
}

grid_ffms () {
    echo "gridding FFMs"
    mkdir grid_search_results
    solvers=(ffm_w_linear ffm_w_linear_reversed)
    l_list_ffms=0,0.003,0.01,0.03
    k_list_ffms=4,8,16,32
    r_list_ffms=0.1,0.2,0.5
    t_ffms=100
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_ffms} ${k_list_ffms} ${r_list_ffms} ${t_ffms} ${s} r
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_ffms} ${k_list_ffms} ${r_list_ffms} ${t_ffms} ${s} r &
        fi
    done
}

grid_fms () {
    echo "gridding FMs"
    mkdir grid_search_results
    solvers=(fm1p25_w_linear fm1p5_w_linear fm1p75_w_linear fm2p0_w_linear fm2p25_w_linear fm2p5_w_linear fm2p75_w_linear fm3p0_w_linear fm1p25_w_linear_neutral fm1p5_w_linear_neutral fm1p75_w_linear_neutral fm2p0_w_linear_neutral fm2p25_w_linear_neutral fm2p5_w_linear_neutral fm2p75_w_linear_neutral fm3p0_w_linear_neutral fm1p25_w_linear_reversed fm1p5_w_linear_reversed fm1p75_w_linear_reversed fm2p0_w_linear_reversed fm2p25_w_linear_reversed fm2p5_w_linear_reversed fm2p75_w_linear_reversed fm3p0_w_linear_reversed)
    l_list_fms=0,0.001,0.003,0.01
    k_list_fms=4,8,16,32
    r_list_fms=0.1,0.2,0.5
    t_fms=100
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_fms} ${k_list_fms} ${r_list_fms} ${t_fms} ${s} r
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_fms} ${k_list_fms} ${r_list_fms} ${t_fms} ${s} r &
        fi
    done
}

grid_xgboost () {
    python grid_xgboost.py
}

symlink_utils
generate_dataset
grid_ffms #2
grid_fms #6+24
grid_linear_and_poly2 #4
grid_xgboost
final_ensembles_regression
final_ensembles_regression_ffm

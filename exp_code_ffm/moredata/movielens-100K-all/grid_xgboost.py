
# coding: utf-8

# In[ ]:

import lightgbm as lgb
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')


# In[ ]:

def get_df(path):
    num_cols = 0
    num_rows = 0
    with open(path) as f_in:
        for line in f_in:
            num_rows += 1
            tokens = line.split()
            num_cols = len(tokens)
    #print((num_rows, num_cols))
    num_cols = 50
    df = np.zeros((num_rows, num_cols))
    r = 0
    with open(path) as f_in:
        for line in f_in:
            tokens = line.split()
            y = tokens[0]
            df[r, num_cols - 1] = float(y)
            for token in tokens[1:]:
                field, feature, val = token.split(':')
                if int(field) < 5:
                    df[r, int(field)] = int(feature)
                else:
                    assert(int(feature) % 2 == 0)
                    df[r, int(field) + int((int(feature) - 2654) / 2)] = int(feature)
            r += 1
            if r % 1000 == 0:
                print("Processed %d lines" %r)
    df = pd.DataFrame(df)
    df.to_csv(path + '.csv', index=False, header=None)
    return df


# In[ ]:

df_tr = get_df('../../moredata/movielens-100K-all/tr.ffm')
df_va = get_df('../../moredata/movielens-100K-all/va.ffm')
df_trva = get_df('../../moredata/movielens-100K-all/trva.ffm')
df_te = get_df('../../moredata/movielens-100K-all/te.ffm')


# In[ ]:

df_tr.head()


# In[ ]:

#df_tr_va_trva_te = pd.concat([df_tr, df_va, df_trva, df_te], axis=0)


# In[ ]:

#for c in df_tr_va_trva_te.columns:
#    df_tr_va_trva_te[c] = df_tr_va_trva_te[c].astype(str)


# In[ ]:

import os
if not os.path.isdir('grid_search_results'):
    os.mkdir('grid_search_results')
path_loss = 'grid_search_results/lightgbm.loss'
path_cmd = 'grid_search_results/lightgbm.cmd'
objective = 'regression'
eval_metric = 'rmse'
learning_rates = [0.1,0.3,1.0]
num_leavess = [5, 10, 30, 100]
reg_lambdas = [0,1,3,10]
n_estimators = 1000
early_stopping_rounds = 100
def calc_loss(pred_te, y_te):
    return np.sqrt(np.mean((pred_te - y_te) ** 2))


# In[ ]:

y_trva = df_trva.iloc[:,-1]
y_tr = df_tr.iloc[:,-1]
y_va = df_va.iloc[:,-1]
y_te = df_te.iloc[:,-1]
df_tr = df_tr.iloc[:,range(df_tr.shape[1] - 1)]
df_va = df_va.iloc[:,range(df_va.shape[1] - 1)]
df_trva = df_trva.iloc[:,range(df_trva.shape[1] - 1)]
df_te = df_te.iloc[:,range(df_te.shape[1] - 1)]


# In[ ]:

results = []
for learning_rate in learning_rates:
    for num_leaves in num_leavess:
        for reg_lambda in reg_lambdas:
            params = {
                'objective':objective,
                'num_leaves':num_leaves, #1000,
                'learning_rate':learning_rate, #0.1,
                'n_estimators':1000, #100,
                'reg_lambda':reg_lambda,
                'silent':False
            }
            model = lgb.LGBMRegressor(**params)
            from sklearn.model_selection import KFold
            eval_set = [(df_va, y_va)]
            order = np.random.permutation(len(y_tr))
            model.fit(df_tr.iloc[order,:], y_tr.iloc[order], verbose=False, early_stopping_rounds = early_stopping_rounds, eval_set = eval_set, eval_metric=eval_metric)
            print([params, model.best_score_, model.best_iteration_])
            results.append([params, model.best_score_, model.best_iteration_])
best_params = None
best_loss = None
best_iter = None
for params, loss, iter in results:
    loss = loss['valid_0']['rmse']
    if best_loss is None or best_loss > loss:
        best_params = params
        best_loss = loss
        best_iter = iter
print('Best params/loss/iteration found:')
[best_params, best_loss, best_iter]


# In[ ]:

params = best_params
params['n_estimators'] = best_iter
model = lgb.LGBMRegressor(**params)
order = np.random.permutation(len(y_trva))
model.fit(df_trva.iloc[order,:], y_trva.iloc[order], verbose=True, eval_set = [(df_te,y_te)], eval_metric=eval_metric)
#model.fit(df_trva, y_trva, verbose=True, eval_set = [(df_te,y_te)], eval_metric=eval_metric)
pred_te = model.predict(X=df_te)
loss = calc_loss(pred_te, y_te)


# In[ ]:

with open(path_loss, 'w+') as f_out:
    f_out.write("%.5f"%loss)
with open(path_cmd, 'w+') as f_out:
    f_out.write(str([best_params, best_loss, best_iter]))


# In[ ]:

pred_te_df = pd.DataFrame(pred_te)


# In[ ]:

pred_te_df.to_csv('pred_xgboost', index=False)


# In[ ]:

p_xgboost = pd.read_csv('pred_xgboost')
p_other = pd.read_csv('grid_search_results/fm1p25_w_linear_neutral.pred', header=None)


# In[ ]:

plt.scatter(p_xgboost, p_other, alpha=0.1)
plt.show()


# In[ ]:

calc_loss(p_other[0], y_te)


# In[ ]:

calc_loss(p_xgboost.iloc[:,0], y_te)


# In[ ]:

np.corrcoef(p_other.iloc[:,0], p_xgboost.iloc[:,0])[0, 1]


# In[ ]:





# coding: utf-8

# In[ ]:

import argparse, sys, math
import pandas as pd
import numpy as np
from sklearn import preprocessing


# In[ ]:

categorical_features = ["user_id"
                         ,"movie_id"
                         ,"age"
                         ,"gender"
                         ,"occupation"
                         ,"Action"
                         ,"Adventure"
                         ,"Animation"
                         ,"Childrens"
                         ,"Comedy"
                         ,"Crime"
                         ,"Documentary"
                         ,"Drama"
                         ,"Fantasy"
                         ,"Film-Noir"
                         ,"Horror"
                         ,"Musical"
                         ,"Mystery"
                         ,"Romance"
                         ,"Sci-Fi"
                         ,"Thriller"
                         ,"War"
                         ,"Western"]


# In[ ]:

ARGS = {'src_path' : '../../data/movielens-100K/ml-100k', 'dst' : 'ml-100k/u.ffm'}


# In[ ]:

users = pd.read_csv(ARGS["src_path"] + "/u.user", sep='|', header=None, names=["user_id","age","gender","occupation","zip_code"])


# In[ ]:

items = pd.read_csv(ARGS["src_path"] + "/u.item", encoding = "ISO-8859-1", sep='|', header=None, names=["movie_id","movie_title","release_date","video_release_date","IMDb_URL","unknown","Action","Adventure","Animation","Childrens","Comedy","Crime","Documentary","Drama","Fantasy","Film-Noir","Horror","Musical","Mystery","Romance","Sci-Fi","Thriller","War","Western"])


# In[ ]:

ratings = pd.read_csv(ARGS["src_path"] + "/u.data", sep='\t', header=None, names=["user_id","movie_id","rating","timestamp"])


# In[ ]:

df = ratings.join(users.set_index('user_id'), on='user_id')


# In[ ]:

df = df.join(items.set_index('movie_id'), on='movie_id')


# In[ ]:

df.head()


# In[ ]:

df = df[categorical_features]


# In[ ]:

df.head()


# In[ ]:

df['age'] = pd.qcut(df['age'], 5, labels=False)


# In[ ]:

df.head()


# In[ ]:

for col in ['gender', 'occupation']:
    le = preprocessing.LabelEncoder()
    le.fit(df[col])
    df[col] = le.transform(df[col])


# In[ ]:

df.head()


# In[ ]:

df['user_id'] = df['user_id'] - 1
df['movie_id'] = df['movie_id'] - 1
df.describe()


# In[ ]:

df_copy = df.copy()


# In[ ]:

offset = 0
for col in df.columns:
    df[col] = df[col] + offset
    offset = max(df[col]) + 1


# In[ ]:

df.head()


# In[ ]:

df.describe()


# In[ ]:

r = ratings['rating']
with open(ARGS['dst'], 'w+') as f:
    tot = 0
    for i in range(len(df)):
        row = str(r[i])
        for j in range(df.shape[1]):
            if j < 5:
                row += " %d:%d:1" %(j, df.iloc[i, j])
            else:
                if df_copy.iloc[i, j] == 1:
                    row += " %d:%d:1" %(5, df.iloc[i, j])
        row += "\n"
        f.write(row)


# In[ ]:




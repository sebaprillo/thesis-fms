#!/usr/bin/env python3

import argparse, sys, math

parser = argparse.ArgumentParser()
parser.add_argument('src', type=str)
parser.add_argument('dst', type=str)
ARGS = vars(parser.parse_args())

num_users = 943
num_items = 1682
USER = 0
ITEM = 1
RATING = 2
TIMESTAMP = 3

with open(ARGS['dst'], 'w+') as f_out:
    with open(ARGS['src']) as f_in:
        total_lines = 0
        for line in f_in:
            total_lines += 1
            line = line.split()
            assert(len(line) == 4)
            user = int(line[USER])
            user -= 1
            assert(0 <= user and user < num_users)
            item = int(line[ITEM])
            item -= 1
            assert(0 <= item and item < num_items)
            rating = int(line[RATING])
            f_out.write("%d 0:%d:1 1:%d:1\n"%(rating, user, num_users + item))
        print("total lines processed = %d" %total_lines)

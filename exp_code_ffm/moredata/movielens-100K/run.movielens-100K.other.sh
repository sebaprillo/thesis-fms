#!/bin/bash

n_thread=36
s=1

#set -e

source ../../util/common.sh

counter=0
dataset_name=movielens-100K

mkdir grid_search_results
mkdir grid_search_results_early_stop_test
mkdir grid_search_results/best_params
mkdir grid_search_results_early_stop_test/best_params

cd ../../
make
cd moredata/${dataset_name}

generate_dataset () {
    echo "Generating ${dataset_name} dataset"
    mkdir ml-100k
    #for i in 1 2 3 4 5
    #do
    #    for what in base test
    #    do
    #        ./to_ffm.py ../../data/movielens-100K/ml-100k/u${i}.${what} ml-100k/u${i}.${what}.ffm
    #    done
    #    ../../util/split.py 0.2 ml-100k/u${i}.base.ffm ml-100k/u${i}.base_tr.ffm ml-100k/u${i}.base_va.ffm
    #done
    ./to_ffm.py ../../data/movielens-100K/ml-100k/u.data ml-100k/u.ffm
    ../../util/split.py 0.2 ml-100k/u.ffm ml-100k/u_trva.ffm ml-100k/u_te.ffm
    ../../util/split.py 0.2 ml-100k/u_trva.ffm ml-100k/u_tr.ffm ml-100k/u_va.ffm
    ln -sf ml-100k/u_tr.ffm tr.ffm
    ln -sf ml-100k/u_va.ffm va.ffm
    ln -sf ml-100k/u_trva.ffm trva.ffm
    ln -sf ml-100k/u_te.ffm te.ffm
    ../../util/ffm2fm_regression.py tr.ffm tr.fm
    ../../util/ffm2fm_regression.py va.ffm va.fm
    ../../util/ffm2fm_regression.py trva.ffm trva.fm
    ../../util/ffm2fm_regression.py te.ffm te.fm
}

grid_fms () {
    echo "gridding FMs"
    mkdir grid_search_results
    solvers=(fm1p25_w_linear fm1p5_w_linear fm1p75_w_linear fm2p0_w_linear fm2p25_w_linear fm2p5_w_linear fm2p75_w_linear fm3p0_w_linear fm1p25_w_linear_neutral fm1p5_w_linear_neutral fm1p75_w_linear_neutral fm2p0_w_linear_neutral fm2p25_w_linear_neutral fm2p5_w_linear_neutral fm2p75_w_linear_neutral fm3p0_w_linear_neutral fm1p25_w_linear_reversed fm1p5_w_linear_reversed fm1p75_w_linear_reversed fm2p0_w_linear_reversed fm2p25_w_linear_reversed fm2p5_w_linear_reversed fm2p75_w_linear_reversed fm3p0_w_linear_reversed)
    l_list_fms=0,0.003,0.03,0.3
    k_list_fms=4,8,16,32
    r_list_fms=0.05,0.1,0.2
    t_fms=200
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_fms} ${k_list_fms} ${r_list_fms} ${t_fms} ${s} "r"
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_fms} ${k_list_fms} ${r_list_fms} ${t_fms} ${s} "r" &
        fi
    done
}

#symlink_utils
#generate_dataset
#
#l=0.03 r=0.1 k=8 sigma=1e-5 task=r t=100
#train_fm
l=0.03 r=0.1 k=32 sigma=1 task=r t=10
train_ffm
#l=0.03 r=0.1 k=8 sigma=1e-5 task=r t=100
#train_linear
#l=0.03 r=0.1 k=8 sigma=1e-5 task=r t=100
#train_poly2
#task=r method=als init_stdev=1e-5 out=output.txt iter=100 dim=1,1,4 regular=5 #Test=0.899404
#train_libfm

#grid_fms
#final_ensembles_regression_parameterized

#!/bin/bash
# requires parallel!
n_thread=36
s=1

#set -e

source ../../util/common.sh

counter=0
dataset_name=ijcnn

mkdir grid_search_results
mkdir grid_search_results/best_params

cd ../../
make
cd moredata/${dataset_name}

generate_dataset () {
    echo "Generating ${dataset_name} dataset"
    ln -sf ../../data/ijcnn/ijcnn1.tr
    ln -sf ../../data/ijcnn/ijcnn1.val
    ln -sf ../../data/ijcnn/ijcnn1.t
    parallel --no-notice -u ::: \
" ./cvt.ijcnn.py ijcnn1.tr ijcnn.tr.ffm " \
" ./cvt.ijcnn.py ijcnn1.val ijcnn.va.ffm " \
" ./cvt.ijcnn.py ijcnn1.t ijcnn.te.ffm "
    cp ijcnn.tr.ffm ijcnn.trva.ffm
    chmod +w ijcnn.trva.ffm
    cat ijcnn.va.ffm >> ijcnn.trva.ffm
    ln -sf ijcnn.tr.ffm tr.ffm
    ln -sf ijcnn.va.ffm va.ffm
    ln -sf ijcnn.trva.ffm trva.ffm
    ln -sf ijcnn.te.ffm te.ffm
}

grid_linear_and_poly2 () {
    echo "gridding LM and Poly2"
    mkdir grid_search_results
    solvers=(linear poly2_w_linear linear_reversed poly2_w_linear_reversed)
    l_list_baselines=0,1e-6,2e-6,5e-6,1e-5,2e-5,5e-5,1e-4,2e-4,5e-4,1e-3,2e-3,5e-3,1e-2,2e-3,5e-3
    k_list_baselines=4 #unused
    r_list_baselines=0.1,0.2,0.5
    t_baselines=200
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_baselines} ${k_list_baselines} ${r_list_baselines} ${t_baselines} ${s}
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_baselines} ${k_list_baselines} ${r_list_baselines} ${t_baselines} ${s} &
        fi
    done
}

grid_ffms () {
    echo "gridding FFMs"
    mkdir grid_search_results
    solvers=(ffm_w_linear ffm_w_linear_reversed)
    l_list_ffms=0,1e-7,1e-6,1e-5
    k_list_ffms=1,2,4,8
    r_list_ffms=0.1,0.2,0.5
    t_ffms=50
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_ffms} ${k_list_ffms} ${r_list_ffms} ${t_ffms} ${s}
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_ffms} ${k_list_ffms} ${r_list_ffms} ${t_ffms} ${s} &
        fi
    done
}

grid_fms () {
    echo "gridding FMs"
    mkdir grid_search_results
    solvers=(fm1p25_w_linear fm1p5_w_linear fm1p75_w_linear fm2p0_w_linear fm2p25_w_linear fm2p5_w_linear fm2p75_w_linear fm3p0_w_linear fm1p25_w_linear_neutral fm1p5_w_linear_neutral fm1p75_w_linear_neutral fm2p0_w_linear_neutral fm2p25_w_linear_neutral fm2p5_w_linear_neutral fm2p75_w_linear_neutral fm3p0_w_linear_neutral fm1p25_w_linear_reversed fm1p5_w_linear_reversed fm1p75_w_linear_reversed fm2p0_w_linear_reversed fm2p25_w_linear_reversed fm2p5_w_linear_reversed fm2p75_w_linear_reversed fm3p0_w_linear_reversed)
    l_list_fms=0,0.00001,0.0001,0.001
    k_list_fms=8,16,32,64
    r_list_fms=0.1,0.2,0.5
    t_fms=50
    for solver in ${solvers[*]}
    do
        let "counter = $counter + 1"
        echo "counter = ${counter}"
        echo "gridding ${solver}"
        if [ "$counter" == "$n_thread" ]; then
            echo "blocking"
            let "counter = 0"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_fms} ${k_list_fms} ${r_list_fms} ${t_fms} ${s}
        else
            echo "spawning thread"
            ./grid_para3.py ${solver} tr.ffm va.ffm trva.ffm te.ffm grid_search_results ${l_list_fms} ${k_list_fms} ${r_list_fms} ${t_fms} ${s} &
        fi
    done
}

grid_xgboost () {
    python grid_xgboost.py
}

symlink_utils
generate_dataset
grid_ffms #2
grid_fms #6+24
grid_linear_and_poly2 #4
grid_xgboost
final_ensembles


# coding: utf-8

# In[ ]:

import xgboost as xgb
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')


# In[ ]:

def get_df(path):
    num_cols = 0
    num_rows = 0
    with open(path) as f_in:
        for line in f_in:
            num_rows += 1
            tokens = line.split()
            y = tokens[0]
            for token in tokens[1:]:
                col, val = token.split(':')
                num_cols = max(num_cols, int(col))
    num_cols += 2
    #print((num_rows, num_cols))
    df = np.zeros(shape=(num_rows, num_cols))
    r = 0
    with open(path) as f_in:
        for line in f_in:
            tokens = line.split()
            y = tokens[0]
            df[r, num_cols - 1] = float(y)
            for token in tokens[1:]:
                col, val = token.split(':')
                df[r, int(col)] = float(val)
            r += 1
    df = pd.DataFrame(df)
    df.to_csv(path + '.csv', index=False, header=None)
    return df


# In[ ]:

df_tr = get_df('../../data/ijcnn/ijcnn1.tr')
df_va = get_df('../../data/ijcnn/ijcnn1.val')
df_te = get_df('../../data/ijcnn/ijcnn1.t')


# In[ ]:

import os
if not os.path.isdir('grid_search_results'):
    os.mkdir('grid_search_results')
path_loss = 'grid_search_results/xgboost.loss'
path_cmd = 'grid_search_results/xgboost.cmd'
objective = 'binary:logistic'
eval_metric = 'logloss'
learning_rates = [0.1] #[0.1,0.3,1.0]
max_depths = [7] #[3,5,7,10]
reg_lambdas = [10] #[0,1,3,10]
n_estimators = 1000
early_stopping_rounds = 100
def calc_loss(pred_te, y_te):
    def normalize(v):
        return np.array([min(max(x,1e-5),1-1e-5) for x in v])
    pred_te = normalize(pred_te)
    y_te = normalize(y_te)
    return -np.mean(np.multiply(y_te, np.log(pred_te)) + (1.0 - y_te) * np.log(1.0 - pred_te))


# In[ ]:

df_trva = pd.concat([df_tr, df_va], axis=0)
y_trva = df_trva.iloc[:,-1] > 0
y_tr = df_tr.iloc[:,-1] > 0
y_va = df_va.iloc[:,-1] > 0
y_te = df_te.iloc[:,-1] > 0
df_tr = df_tr.iloc[:,range(df_tr.shape[1] - 1)]
df_va = df_va.iloc[:,range(df_va.shape[1] - 1)]
df_trva = df_trva.iloc[:,range(df_trva.shape[1] - 1)]
df_te = df_te.iloc[:,range(df_te.shape[1] - 1)]


# In[ ]:

results = []
for learning_rate in learning_rates:
    for max_depth in max_depths:
        for reg_lambda in reg_lambdas:
            params = {
                'objective':objective, 
                'learning_rate':learning_rate,
                'max_depth':max_depth, 
                'reg_lambda':reg_lambda,
                'n_estimators':n_estimators,
            }
            model = xgb.XGBRegressor(**params)
            from sklearn.model_selection import KFold
            eval_set = [(df_va, y_va)]
            order = np.random.permutation(len(y_tr))
            df_tr2 = df_tr.iloc[order,:].copy()
            y_tr2 = y_tr.iloc[order].copy()
            model.fit(df_tr2, y_tr2, verbose=False, early_stopping_rounds = early_stopping_rounds, eval_set = eval_set, eval_metric=eval_metric)
            print([params, model.best_score, model.best_iteration])
            results.append([params, model.best_score, model.best_iteration])
best_params = None
best_loss = None
best_iter = None
for params, loss, iter in results:
    if best_loss is None or best_loss > loss:
        best_params = params
        best_loss = loss
        best_iter = iter
print('Best params/loss/iteration found:')
[best_params, best_loss, best_iter]


# In[ ]:

params = best_params
params['n_estimators'] = best_iter
model = xgb.XGBRegressor(**params)
order = np.random.permutation(len(y_trva))
df_trva2 = df_trva.iloc[order,:].copy()
y_trva2 = y_trva.iloc[order].copy()
model.fit(df_trva2, y_trva2, verbose=True, eval_set = [(df_te,y_te)], eval_metric=eval_metric)
#model.fit(df_trva, y_trva, verbose=True, eval_set = [(df_te,y_te)], eval_metric=eval_metric)
pred_te = model.predict(data=df_te)
loss = calc_loss(pred_te, y_te)


# In[ ]:

with open(path_loss, 'w+') as f_out:
    f_out.write("%.5f"%loss)
with open(path_cmd, 'w+') as f_out:
    f_out.write(str([best_params, best_loss, best_iter]))


# In[ ]:

pred_te_df = pd.DataFrame(pred_te)


# In[ ]:

pred_te_df.to_csv('pred_xgboost', index=False)


# In[ ]:

p_xgboost = pd.read_csv('pred_xgboost')
p_other = pd.read_csv('grid_search_results/fm1p25_w_linear_neutral.pred', header=None)


# In[ ]:

plt.scatter(p_xgboost, p_other, alpha=0.1)
plt.show()


# In[ ]:

calc_loss(p_other[0], y_te)


# In[ ]:

calc_loss(p_xgboost.iloc[:,0], y_te)


# In[ ]:

np.corrcoef(p_other.iloc[:,0], p_xgboost.iloc[:,0])[0, 1]


# In[ ]:




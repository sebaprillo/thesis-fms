# This script creates all fm_core solver directories, then their Makefile,
# flagged for gradient checking! Makes clean to whipe any pre-existing binaries
# for this model.

orders=(1p25 1p5 1p75 2p0 2p25 2p5 2p75 3p0)
D_orders=(FM_1P25 FM_1P5 FM_1P75 FM_2P0 FM_2P25 FM_2P5 FM_2P75 FM_3P0)
symmetries=('' '_neutral' '_reversed')
D_symmetries=(FM_STANDARD FM_NEUTRAL FM_REVERSED)
diags=('' '_no_diag')
D_diags=(FM_DIAG FM_NO_DIAG)

create_makefile () {
    echo "creating $1/Makefile"
    touch $1/Makefile
    echo "SOLVER_NAME = $1
SOLVER_CORE = fm_core
CXX = g++
CXXFLAGS = -Wall -O3 -std=c++0x -march=native

DFLAG = -DUSESSE -D$2 -D$3 -D$4 -DFM_CHECK_GRADIENTS -DFM_USE_LINEAR

# comment the following flags if you do not want to use OpenMP
DFLAG += -DUSEOMP
CXXFLAGS += -fopenmp

all: \$(SOLVER_NAME)-train \$(SOLVER_NAME)-predict

\$(SOLVER_NAME)-train: ../\$(SOLVER_CORE)/ffm-train.cpp ffm.o timer.o
	\$(CXX) \$(CXXFLAGS) -o \$@ \$^

\$(SOLVER_NAME)-predict: ../\$(SOLVER_CORE)/ffm-predict.cpp ffm.o timer.o
	\$(CXX) \$(CXXFLAGS) -o \$@ \$^

ffm.o: ../\$(SOLVER_CORE)/ffm.cpp ../\$(SOLVER_CORE)/ffm.h timer.o
	\$(CXX) \$(CXXFLAGS) \$(DFLAG) -c -o \$@ \$<

timer.o: ../\$(SOLVER_CORE)/timer.cpp ../\$(SOLVER_CORE)/timer.h
	\$(CXX) \$(CXXFLAGS) \$(DFLAG) -c -o \$@ \$<

clean:
	rm -f \$(SOLVER_NAME)-train \$(SOLVER_NAME)-predict ffm.o timer.o
" > $1/Makefile
    cd $1
    make clean
    cd ..
}

for ((idx_order=0; idx_order < ${#orders[@]} ; idx_order++))
do
    for ((idx_symmetries=0; idx_symmetries < ${#symmetries[@]} ; idx_symmetries++))
    do
        for ((idx_diag=0; idx_diag < ${#diags[@]} ; idx_diag++))
        do
            order=${orders[${idx_order}]}
            D_order=${D_orders[${idx_order}]}
            symmetry=${symmetries[${idx_symmetries}]}
            D_symmetry=${D_symmetries[${idx_symmetries}]}
            diag=${diags[${idx_diag}]}
            D_diag=${D_diags[${idx_diag}]}
            solver_name=fm${order}_w_linear${symmetry}${diag}
            echo "creating directory ${solver_name}"
            mkdir ${solver_name}
            create_makefile ${solver_name} ${D_order} ${D_symmetry} ${D_diag}
        done
    done
done

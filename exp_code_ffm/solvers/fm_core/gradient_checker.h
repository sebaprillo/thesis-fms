inline ffm_float wTx_for_gradient_checker(
    ffm_node *begin,
    ffm_node *end,
    ffm_float r,
    ffm_model &model, 
    ffm_float kappa=0, 
    ffm_float eta=0, 
    ffm_float lambda=0, 
    bool do_update=false,
    ffm_int params_per_weight=2)
{
    assert(do_update);
    ffm_long align0 = (ffm_long)model.k*params_per_weight;

    // Gradients will be 0 except when the feature is on, so initialize all to 0 by default, which is the correct gradient value.
    for(int i = 0; i <= 0; i++) model.WB[2 * i + 1] = 0.0;
    for(int i = 0; i < model.n; i++) model.WL[2 * i + 1] = 0.0;
    for(int i = 0; i < model.n; i++)
        for(int j = 0; j < model.k; j++)
            model.W[i * align0 + model.k + j] = 0.0;

    __m128 XMMsign = _mm_load_ps(model.sign);
    __m128 XMMkappa_sign_orderdiv2 = _mm_mul_ps(_mm_set1_ps(kappa), XMMsign);
    __m128 XMMlambda = _mm_set1_ps(lambda);

#if defined(FM_1P25)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(1.25f / 2.0f));
#elif defined(FM_1P5)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(1.5f / 2.0f));
#elif defined(FM_1P75)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(1.75f / 2.0f));
#elif defined(FM_2P0)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(2.0f / 2.0f));
#elif defined(FM_2P25)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(2.25f / 2.0f));
#elif defined(FM_2P5)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(2.5f / 2.0f));
#elif defined(FM_2P75)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(2.75f / 2.0f));
#elif defined(FM_3P0)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(3.0f / 2.0f));
#else
    error_FM_order_must_be_defined
#endif

    std::vector<float>   sv(model.k, 0);
    ffm_float *s =       sv.data();
    std::vector<float>   ssignv(model.k, 0);
    ffm_float *ssign =   ssignv.data();
    std::vector<float>   sabsv(model.k, 0);
    ffm_float *sabs =    sabsv.data();
    std::vector<float>   som1v(model.k, 0);
    ffm_float *som1 =    som1v.data();
    std::vector<float>   sov(model.k, 0);
    ffm_float *so =      sov.data();
#ifndef FM_NO_DIAG
    std::vector<float>   s2v(model.k, 0);
    ffm_float *s2 =      s2v.data();
    std::vector<float>   s2od2m1v(model.k, 0);
    ffm_float *s2od2m1 = s2od2m1v.data();
    std::vector<float>   s2od2v(model.k, 0);
    ffm_float *s2od2 =   s2od2v.data();
#endif

    ffm_float t = 0;

    for(ffm_node *N1 = begin; N1 != end; N1++)
    {
        ffm_int j1 = N1->j;
        ffm_float v1 = N1->v;
        if(j1 >= model.n)
            continue;

        ffm_float *w = model.W + j1 * align0;

        __m128 XMMv = _mm_set1_ps(v1*r);

        for(ffm_int d = 0; d < model.k; d += 4)
        {
            __m128 XMMs = _mm_load_ps(s+d);
#ifndef FM_NO_DIAG
            __m128 XMMs2 = _mm_load_ps(s2+d);
#endif
            __m128 const XMMw = _mm_load_ps(w+d);

            __m128 XMMwv = _mm_mul_ps(XMMw, XMMv);
            XMMs = _mm_add_ps(XMMs, XMMwv);
#ifndef FM_NO_DIAG
            XMMs2 = _mm_add_ps(XMMs2, _mm_mul_ps(XMMwv, XMMwv));
#endif

            _mm_store_ps(s+d, XMMs);
#ifndef FM_NO_DIAG
            _mm_store_ps(s2+d, XMMs2);
#endif
        }

    }

    for(ffm_int d = 0; d < model.k; d++)
    {
        ssign[d] = s[d] > 0.0 ? 1.0 : -1.0;
        sabs[d] = s[d] * ssign[d];
    }

    for(ffm_int d = 0; d < model.k; d += 4)
    {
		__m128 XMMsabs = _mm_load_ps(sabs+d);
#if defined(FM_1P25)
        __m128 XMMsom1 = _mm_sqrt_ps(_mm_sqrt_ps(XMMsabs)); // 0.25
#elif defined(FM_1P5)
        __m128 XMMsom1 = _mm_sqrt_ps(XMMsabs); // 0.5
#elif defined(FM_1P75)
        __m128 XMMsom1 = _mm_sqrt_ps(_mm_mul_ps(XMMsabs, _mm_sqrt_ps(XMMsabs))); // 0.75
#elif defined(FM_2P0)
        __m128 XMMsom1 = XMMsabs; // 1.0
#elif defined(FM_2P25)
        __m128 XMMsom1 = _mm_mul_ps(XMMsabs, _mm_sqrt_ps(_mm_sqrt_ps(XMMsabs))); // 1.25
#elif defined(FM_2P5)
        __m128 XMMsom1 = _mm_mul_ps(XMMsabs, _mm_sqrt_ps(XMMsabs)); // 1.5
#elif defined(FM_2P75)
        __m128 XMMsom1 = _mm_mul_ps(XMMsabs, _mm_sqrt_ps(_mm_mul_ps(XMMsabs, _mm_sqrt_ps(XMMsabs)))); // 1.75
#elif defined(FM_3P0)
        __m128 XMMsom1 = _mm_mul_ps(XMMsabs, XMMsabs); // 2.0
#else
        error_FM_order_must_be_defined
#endif
		__m128 XMMso = _mm_mul_ps(XMMsom1, XMMsabs);
#ifndef FM_NO_DIAG
        __m128 XMMs2 = _mm_load_ps(s2+d);
#if defined(FM_1P25)
        __m128 XMMs2od2m1 = _mm_rsqrt_ps(_mm_sqrt_ps(_mm_mul_ps(XMMs2, _mm_sqrt_ps(XMMs2)))); // -3/8
#elif defined(FM_1P5)
        __m128 XMMs2od2m1 = _mm_rsqrt_ps(_mm_sqrt_ps(XMMs2)); // -1/4
#elif defined(FM_1P75)
        __m128 XMMs2od2m1 = _mm_rsqrt_ps(_mm_sqrt_ps(_mm_sqrt_ps(XMMs2))); // -1/8
#elif defined(FM_2P0)
        __m128 XMMs2od2m1 = _mm_set1_ps(1.0f); // 0
#elif defined(FM_2P25)
        __m128 XMMs2od2m1 = _mm_sqrt_ps(_mm_sqrt_ps(_mm_sqrt_ps(XMMs2))); // 1/8
#elif defined(FM_2P5)
        __m128 XMMs2od2m1 = _mm_sqrt_ps(_mm_sqrt_ps(XMMs2)); // 1/4
#elif defined(FM_2P75)
        __m128 XMMs2od2m1 = _mm_sqrt_ps(_mm_sqrt_ps(_mm_mul_ps(XMMs2, _mm_sqrt_ps(XMMs2)))); // 3/8
#elif defined(FM_3P0)
        __m128 XMMs2od2m1 = _mm_sqrt_ps(XMMs2); // 0.5
#else
        error_FM_order_must_be_defined
#endif
		__m128 XMMs2od2 = _mm_mul_ps(XMMs2od2m1, XMMs2);
#endif // FM_NO_DIAG
		_mm_store_ps(som1+d, XMMsom1);
		_mm_store_ps(so+d, XMMso);
#ifndef FM_NO_DIAG
		_mm_store_ps(s2od2m1+d, XMMs2od2m1);
		_mm_store_ps(s2od2+d, XMMs2od2);
#endif
	}

    __m128 XMMt = _mm_set1_ps(0.0f);
    for(ffm_node *N1 = begin; N1 != end; N1++)
    {
        ffm_int j1 = N1->j;
        ffm_float v1 = N1->v;
        if(j1 >= model.n)
            continue;

        ffm_float *w = model.W + j1 * align0;

        __m128 XMMv = _mm_set1_ps(v1*r);

        ffm_float &wl = model.WL[j1 * params_per_weight];
        if(do_update)
        {
            ffm_float &wlg = model.WL[j1 * params_per_weight + 1 ];
            ffm_float g = lambda * wl + kappa * v1 * r;
            wlg = g; // We store the gradient instead of the Adagrad parameter
        }
        else
        {
            assert(false);
            t += wl * v1 * r;
        }

        if(do_update)
        {
            __m128 XMMkappav_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, XMMv);
            for(ffm_int d = 0; d < model.k; d += 4)
            {
                __m128 XMMssign = _mm_load_ps(ssign+d);
                __m128 XMMsom1 = _mm_load_ps(som1+d);
                __m128 XMMw = _mm_load_ps(w+d);
#ifndef FM_NO_DIAG
                __m128 XMMs2od2m1 = _mm_load_ps(s2od2m1+d);
                __m128 XMMg = _mm_add_ps(_mm_mul_ps(XMMlambda, XMMw), 
                    _mm_mul_ps(XMMkappav_sign_orderdiv2, _mm_sub_ps(_mm_mul_ps(XMMsom1, XMMssign), _mm_mul_ps(_mm_mul_ps(XMMw, XMMv), XMMs2od2m1))));
#else
                __m128 XMMg = _mm_add_ps(_mm_mul_ps(XMMlambda, XMMw), 
                    _mm_mul_ps(XMMkappav_sign_orderdiv2, _mm_mul_ps(XMMsom1, XMMssign)));
#endif

                _mm_store_ps(w+model.k+d, XMMg); // We store the gradient instead of the Adagrad parameter
            }
        }
    }
    if(!do_update)
    {
        assert(false);
        for(ffm_int d = 0; d < model.k; d += 4)
        {
            __m128 XMMso = _mm_load_ps(so+d);
#ifndef FM_NO_DIAG
            __m128 XMMs2od2 = _mm_load_ps(s2od2+d);
            XMMt = _mm_add_ps(XMMt, _mm_mul_ps(_mm_set1_ps(0.5f), _mm_mul_ps(XMMsign, _mm_sub_ps(XMMso, XMMs2od2))));
#else
            XMMt = _mm_add_ps(XMMt, _mm_mul_ps(_mm_set1_ps(0.5f), _mm_mul_ps(XMMsign, XMMso)));
#endif
        }
    }


    ffm_float &wb = model.WB[0];
    if(do_update)
    {
        ffm_float &wbg = model.WB[1];
        ffm_float g = kappa;
        wbg = g; // We store the gradient instead of the Adagrad parameter
    }
    else
    {
        assert(false);
        t += wb;
    }

    if(do_update)
        return 0;

    XMMt = _mm_hadd_ps(XMMt, XMMt);
    XMMt = _mm_hadd_ps(XMMt, XMMt);
    ffm_float t_all;
    _mm_store_ss(&t_all, XMMt);
    t_all += t;

    return t_all;
}

ffm_float loss_regularization(ffm_node *begin, ffm_node *end, ffm_model &model, ffm_float lambda)
{
    ffm_float reg = 0.0;
    //reg += (model.W[0]) * (model.W[0]);// Bias term is not penalized
    for(ffm_node *N1 = begin; N1 != end; N1++) // TODO: Only adds up L2 loss over active weights??? This is not what is reported in the paper!
    {
        ffm_int j1 = N1->j;
        if(j1 >= model.n)
            continue;
        ffm_float wl = model.WL[j1 * 2];
        reg += wl * wl;
        for(int i = 0; i < model.k; i++){
            reg += (model.W[2 * model.k * j1 + i]) * (model.W[2 * model.k * j1 + i]);
        }
    }
    // Note that we only add regulaization for the features that appear! Thus, the following would break the gradients:
    /*for(int j = 0; j < model.n; j++){
        for(int i = 0; i < model.k; i++){
            reg += (model.W[2 * model.k * j + i]) * (model.W[2 * model.k * j + i]);
        }
    }*/
    return 0.5 * lambda * reg;
}

void check_gradient(ffm_float *what, ffm_float *what_grad, ffm_model &model, ffm_node *begin, ffm_node *end, ffm_float kappa, ffm_parameter param, ffm_float y, ffm_float r, ffm_int w_size, ffm_int iter, ffm_int ii, bool kill_kappa, ffm_int key, ffm_int& incorrect_gradients, ffm_int& total_gradients, ffm_int& incorrect_gradient_signs)
{
    total_gradients++; // increase counter for number of gradient checks

    ffm_float epsilon = 0.001;
    *what += epsilon;
    ffm_float t_plus = wTx(begin, end, r, model);
    ffm_float loss_plus = 1e8;
    if(kill_kappa){
        loss_plus = t_plus + loss_regularization(begin, end, model, param.lambda);
    } else {
        ffm_float expnyt_plus = exp(-y*t_plus);
        loss_plus = log(1+expnyt_plus) + loss_regularization(begin, end, model, param.lambda);
    }
    *what -= epsilon;

    *what -= epsilon;
    ffm_float t_neg = wTx(begin, end, r, model);
    ffm_float loss_neg = -1e8;
    if(kill_kappa){
        loss_neg = t_neg + loss_regularization(begin, end, model, param.lambda);
    } else {
        ffm_float expnyt_neg = exp(-y*t_neg);
        loss_neg = log(1+expnyt_neg) + loss_regularization(begin, end, model, param.lambda);
    }
    *what += epsilon;

    ffm_float numerical_gradient = (loss_plus - loss_neg) / (2.0 * epsilon);

    vector<ffm_float> tmp_WB(2, 0);
    vector<ffm_float> tmp_WL(2 * (model.n), 0);
    vector<ffm_float> tmp_W(w_size, 0);
    memcpy(tmp_WB.data(), model.WB, 2*sizeof(ffm_float));
    memcpy(tmp_WL.data(), model.WL, 2*(model.n)*sizeof(ffm_float));
    memcpy(tmp_W.data(), model.W, w_size*sizeof(ffm_float));
    wTx_for_gradient_checker(begin, end, r, model, kill_kappa ? 1.0f : kappa, param.eta, param.lambda, true);
    ffm_float analytic_gradient = *what_grad;
    memcpy(model.WB, tmp_WB.data(), 2*sizeof(ffm_float));
    memcpy(model.WL, tmp_WL.data(), 2*(model.n)*sizeof(ffm_float));
    memcpy(model.W, tmp_W.data(), w_size*sizeof(ffm_float));

    ffm_float abs_error = abs(numerical_gradient - analytic_gradient);
    ffm_float rel_error = abs_error / (max(abs(numerical_gradient), abs(analytic_gradient)) + 1e-9);
    if(numerical_gradient * analytic_gradient < 0.0) incorrect_gradient_signs++;
    if(abs_error > 0.001)// && rel_error > 0.10)
    {
        incorrect_gradients++;
        /*cerr << "iter = " << iter << endl;
        cerr << "ii = " << ii << endl;
        cerr << "key = " << key << endl;
        cerr << "t_plus = " << t_plus << endl;
        cerr << "t_neg = " << t_neg << endl;
        cerr << "r = " << r << endl;
        cerr << "t = " << wTx(begin, end, r, model) << endl;
        cerr << "model.n = " << model.n << endl;
        cerr << "Gradient error: analytic_gradient = " << analytic_gradient << ",  numerical_gradient = " << numerical_gradient << endl;
        assert(false);*/
    }
}

void check_gradients(ffm_model &model, ffm_node *begin, ffm_node *end, ffm_float kappa, ffm_parameter param, ffm_float y, ffm_float r, ffm_int w_size, ffm_int iter, ffm_int ii, bool kill_kappa, ffm_int& incorrect_gradients, ffm_int& total_gradients, ffm_int& incorrect_gradient_signs)
{
    ffm_int key = 0;
    for(int z = 0; z <= 0; z++, key++){
        ffm_float *what = &(model.WB[2 * z]);
        ffm_float *what_grad = what + 1;
        check_gradient(what, what_grad, model, begin, end, kappa, param, y, r, w_size, iter, ii, kill_kappa, key, incorrect_gradients, total_gradients, incorrect_gradient_signs);
    }
    for(int z = 0; z < model.n; z++, key++){
        ffm_float *what = &(model.WL[2 * z]);
        ffm_float *what_grad = what + 1;
        check_gradient(what, what_grad, model, begin, end, kappa, param, y, r, w_size, iter, ii, kill_kappa, key, incorrect_gradients, total_gradients, incorrect_gradient_signs);
    }
    for(int z = 0; z < model.n; z++, key++){
        for(int zz = 0; zz < model.k; zz++, key++){
            ffm_float *what = &(model.W[z * 2 * model.k + zz]);
            ffm_float *what_grad = &(model.W[z * 2 * model.k + zz + model.k]);
            check_gradient(what, what_grad, model, begin, end, kappa, param, y, r, w_size, iter, ii, kill_kappa, key, incorrect_gradients, total_gradients, incorrect_gradient_signs);
        }
    }
}

#pragma GCC diagnostic ignored "-Wunused-result" 
#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <new>
#include <memory>
#include <random>
#include <stdexcept>
#include <string>
#include <cstring>
#include <vector>
#include <pmmintrin.h>
#ifdef FM_CHECK_GRADIENTS
#include <assert.h>
#endif

#if defined USEOMP
#include <omp.h>
#endif

#include "ffm.h"
#include "timer.h"

namespace ffm {

namespace {

using namespace std;

ffm_int const kALIGNByte = 16;
ffm_int const kALIGN = kALIGNByte/sizeof(ffm_float);
ffm_int const kCHUNK_SIZE = 10000000;
ffm_int const kMaxLineSize = 100000;

inline float qrsqrt(float x)
{
    _mm_store_ss(&x, _mm_rsqrt_ps(_mm_load1_ps(&x)));
    return x;
}

inline ffm_float wTx(
    ffm_node *begin,
    ffm_node *end,
    ffm_float r,
    ffm_model &model, 
    ffm_float kappa=0, 
    ffm_float eta=0, 
    ffm_float lambda=0, 
    bool do_update=false,
    ffm_int params_per_weight=2)
{
    ffm_long align0 = (ffm_long)model.k*params_per_weight;

    __m128 XMMsign = _mm_load_ps(model.sign);
    __m128 XMMkappa_sign_orderdiv2 = _mm_mul_ps(_mm_set1_ps(kappa), XMMsign);
    __m128 XMMeta = _mm_set1_ps(eta);
    __m128 XMMlambda = _mm_set1_ps(lambda);

#if defined(FM_1P0)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(1.0f / 2.0f));
#elif defined(FM_1P25)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(1.25f / 2.0f));
#elif defined(FM_1P5)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(1.5f / 2.0f));
#elif defined(FM_1P75)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(1.75f / 2.0f));
#elif defined(FM_2P0)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(2.0f / 2.0f));
#elif defined(FM_2P25)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(2.25f / 2.0f));
#elif defined(FM_2P5)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(2.5f / 2.0f));
#elif defined(FM_2P75)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(2.75f / 2.0f));
#elif defined(FM_3P0)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(3.0f / 2.0f));
#elif defined(FM_RELU)
    XMMkappa_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, _mm_set1_ps(0.5f)); // Divido por 2 para que se parezca lo mas posible a FM_1P0
#else
    error_FM_order_must_be_defined
#endif

    std::vector<float>   sv(model.k, 0);
    ffm_float *s =       sv.data();
    std::vector<float>   ssignv(model.k, 0);
    ffm_float *ssign =   ssignv.data();
    std::vector<float>   sabsv(model.k, 0);
    ffm_float *sabs =    sabsv.data();
    std::vector<float>   som1v(model.k, 0);
    ffm_float *som1 =    som1v.data();
    std::vector<float>   sov(model.k, 0);
    ffm_float *so =      sov.data();
#ifndef FM_NO_DIAG
    std::vector<float>   s2v(model.k, 0);
    ffm_float *s2 =      s2v.data();
    std::vector<float>   s2od2m1v(model.k, 0);
    ffm_float *s2od2m1 = s2od2m1v.data();
    std::vector<float>   s2od2v(model.k, 0);
    ffm_float *s2od2 =   s2od2v.data();
#endif

    ffm_float t = 0;

    for(ffm_node *N1 = begin; N1 != end; N1++)
    {
        ffm_int j1 = N1->j;
        ffm_float v1 = N1->v;
        if(j1 >= model.n)
            continue;

        ffm_float *w = model.W + j1 * align0;

        __m128 XMMv = _mm_set1_ps(v1*r);

        for(ffm_int d = 0; d < model.k; d += 4)
        {
            __m128 XMMs = _mm_load_ps(s+d);
#ifndef FM_NO_DIAG
            __m128 XMMs2 = _mm_load_ps(s2+d);
#endif
            __m128 const XMMw = _mm_load_ps(w+d);

            __m128 XMMwv = _mm_mul_ps(XMMw, XMMv);
            XMMs = _mm_add_ps(XMMs, XMMwv);
#ifndef FM_NO_DIAG
            XMMs2 = _mm_add_ps(XMMs2, _mm_mul_ps(XMMwv, XMMwv));
#endif

            _mm_store_ps(s+d, XMMs);
#ifndef FM_NO_DIAG
            _mm_store_ps(s2+d, XMMs2);
#endif
        }

    }

    for(ffm_int d = 0; d < model.k; d++)
    {
#if defined(FM_RELU)
        ssign[d] = s[d] > 0.0 ? 1.0 : 0.0; // hack para que el gradiente de RELU de 0 si es negativo
        sabs[d] = s[d] > 0.0 ? s[d] : -s[d];
#else
        ssign[d] = s[d] > 0.0 ? 1.0 : -1.0;
        sabs[d] = s[d] * ssign[d];
#endif
    }

    for(ffm_int d = 0; d < model.k; d += 4)
    {
		__m128 XMMsabs = _mm_load_ps(sabs+d);
#if defined(FM_1P0)
        __m128 XMMsom1 = _mm_set1_ps(1.0f); // 0.0
#elif defined(FM_1P25)
        __m128 XMMsom1 = _mm_sqrt_ps(_mm_sqrt_ps(XMMsabs)); // 0.25
#elif defined(FM_1P5)
        __m128 XMMsom1 = _mm_sqrt_ps(XMMsabs); // 0.5
#elif defined(FM_1P75)
        __m128 XMMsom1 = _mm_sqrt_ps(_mm_mul_ps(XMMsabs, _mm_sqrt_ps(XMMsabs))); // 0.75
#elif defined(FM_2P0)
        __m128 XMMsom1 = XMMsabs; // 1.0
#elif defined(FM_2P25)
        __m128 XMMsom1 = _mm_mul_ps(XMMsabs, _mm_sqrt_ps(_mm_sqrt_ps(XMMsabs))); // 1.25
#elif defined(FM_2P5)
        __m128 XMMsom1 = _mm_mul_ps(XMMsabs, _mm_sqrt_ps(XMMsabs)); // 1.5
#elif defined(FM_2P75)
        __m128 XMMsom1 = _mm_mul_ps(XMMsabs, _mm_sqrt_ps(_mm_mul_ps(XMMsabs, _mm_sqrt_ps(XMMsabs)))); // 1.75
#elif defined(FM_3P0)
        __m128 XMMsom1 = _mm_mul_ps(XMMsabs, XMMsabs); // 2.0
#elif defined(FM_RELU)
        __m128 XMMsom1 = _mm_set1_ps(1.0f); // 2.0
#else
        error_FM_order_must_be_defined
#endif
		__m128 XMMso = _mm_mul_ps(XMMsom1, XMMsabs);
#ifndef FM_NO_DIAG
        __m128 XMMs2 = _mm_load_ps(s2+d);
#if defined(FM_1P0)
        __m128 XMMs2od2m1 = _mm_rsqrt_ps(XMMs2); // -1/2
#elif defined(FM_1P25)
        __m128 XMMs2od2m1 = _mm_rsqrt_ps(_mm_sqrt_ps(_mm_mul_ps(XMMs2, _mm_sqrt_ps(XMMs2)))); // -3/8
#elif defined(FM_1P5)
        __m128 XMMs2od2m1 = _mm_rsqrt_ps(_mm_sqrt_ps(XMMs2)); // -1/4
#elif defined(FM_1P75)
        __m128 XMMs2od2m1 = _mm_rsqrt_ps(_mm_sqrt_ps(_mm_sqrt_ps(XMMs2))); // -1/8
#elif defined(FM_2P0)
        __m128 XMMs2od2m1 = _mm_set1_ps(1.0f); // 0
#elif defined(FM_2P25)
        __m128 XMMs2od2m1 = _mm_sqrt_ps(_mm_sqrt_ps(_mm_sqrt_ps(XMMs2))); // 1/8
#elif defined(FM_2P5)
        __m128 XMMs2od2m1 = _mm_sqrt_ps(_mm_sqrt_ps(XMMs2)); // 1/4
#elif defined(FM_2P75)
        __m128 XMMs2od2m1 = _mm_sqrt_ps(_mm_sqrt_ps(_mm_mul_ps(XMMs2, _mm_sqrt_ps(XMMs2)))); // 3/8
#elif defined(FM_3P0)
        __m128 XMMs2od2m1 = _mm_sqrt_ps(XMMs2); // 0.5
#elif defined(FM_RELU)
        __m128 XMMs2od2m1 = _mm_set1_ps(0.0f); // No dejo usar la diagonal!
#else
        error_FM_order_must_be_defined
#endif
		__m128 XMMs2od2 = _mm_mul_ps(XMMs2od2m1, XMMs2);
#endif // FM_NO_DIAG
		_mm_store_ps(som1+d, XMMsom1);
		_mm_store_ps(so+d, XMMso);
#ifndef FM_NO_DIAG
		_mm_store_ps(s2od2m1+d, XMMs2od2m1);
		_mm_store_ps(s2od2+d, XMMs2od2);
#endif
	}

    __m128 XMMt = _mm_set1_ps(0.0f);
    for(ffm_node *N1 = begin; N1 != end; N1++)
    {
        ffm_int j1 = N1->j;
        ffm_float v1 = N1->v;
        ffm_float prob1 = N1->feature_prob;
        if(j1 >= model.n)
            continue;

        ffm_float *w = model.W + j1 * align0;

        __m128 XMMv = _mm_set1_ps(v1*r);
        __m128 XMMfeature_prob = _mm_set1_ps(prob1);

#ifdef FM_USE_LINEAR
        ffm_float &wl = model.WL[j1 * params_per_weight];
        if(do_update)
        {
            ffm_float &wlg = model.WL[j1 * params_per_weight + 1 ];
            ffm_float g = lambda * wl / prob1 + kappa * v1 * r;
            wlg += g * g;
            wl -= eta * qrsqrt(wlg) * g;
        }
        else
        {
            t += wl * v1 * r;
        }
#endif

        if(do_update)
        {
            __m128 XMMkappav_sign_orderdiv2 = _mm_mul_ps(XMMkappa_sign_orderdiv2, XMMv);
            for(ffm_int d = 0; d < model.k; d += 4)
            {
                __m128 XMMssign = _mm_load_ps(ssign+d);
                __m128 XMMsom1 = _mm_load_ps(som1+d);
                __m128 XMMw = _mm_load_ps(w+d);
                __m128 XMMwg = _mm_load_ps(w+model.k+d);
#ifndef FM_NO_DIAG
                __m128 XMMs2od2m1 = _mm_load_ps(s2od2m1+d);
                __m128 XMMg = _mm_add_ps(_mm_div_ps(_mm_mul_ps(XMMlambda, XMMw), XMMfeature_prob),
                    _mm_mul_ps(XMMkappav_sign_orderdiv2, _mm_sub_ps(_mm_mul_ps(XMMsom1, XMMssign), _mm_mul_ps(_mm_mul_ps(XMMw, XMMv), XMMs2od2m1))));
#else
                __m128 XMMg = _mm_add_ps(_mm_div_ps(_mm_mul_ps(XMMlambda, XMMw), XMMfeature_prob),
                    _mm_mul_ps(XMMkappav_sign_orderdiv2, _mm_mul_ps(XMMsom1, XMMssign)));
#endif

                XMMwg = _mm_add_ps(XMMwg, _mm_mul_ps(XMMg, XMMg));

                XMMw = _mm_sub_ps(XMMw,
                    _mm_mul_ps(XMMeta, 
                    _mm_mul_ps(_mm_rsqrt_ps(XMMwg), XMMg)));

                _mm_store_ps(w+d, XMMw);
                _mm_store_ps(w+model.k+d, XMMwg);
            }
        }
    }
    if(!do_update)
    {
        for(ffm_int d = 0; d < model.k; d += 4)
        {
            __m128 XMMso = _mm_load_ps(so+d);
#ifndef FM_NO_DIAG
            __m128 XMMs2od2 = _mm_load_ps(s2od2+d);
            XMMt = _mm_add_ps(XMMt, _mm_mul_ps(_mm_set1_ps(1.0f), _mm_mul_ps(XMMsign, _mm_sub_ps(XMMso, XMMs2od2))));
#else
            XMMt = _mm_add_ps(XMMt, _mm_mul_ps(_mm_set1_ps(1.0f), _mm_mul_ps(XMMsign, XMMso)));
#endif
        }
    }

#ifdef FM_USE_LINEAR
    ffm_float &wb = model.WB[0];
    if(do_update)
    {
        ffm_float &wbg = model.WB[1];
        ffm_float g = kappa;
        wbg += g * g;
        wb -= eta * qrsqrt(wbg) * g;
    }
    else
    {
        t += wb;
    }
#endif

    if(do_update)
        return 0;

    XMMt = _mm_hadd_ps(XMMt, XMMt);
    XMMt = _mm_hadd_ps(XMMt, XMMt);
    ffm_float t_all;
    _mm_store_ss(&t_all, XMMt);
    t_all += t;

    return t_all;
}

#ifdef FM_CHECK_GRADIENTS
#include "gradient_checker.h"
#endif

ffm_float* malloc_aligned_float(ffm_long size)
{
    void *ptr;

#ifdef _WIN32
    ptr = _aligned_malloc(size*sizeof(ffm_float), kALIGNByte);
    if(ptr == nullptr)
        throw bad_alloc();
#else
    int status = posix_memalign(&ptr, kALIGNByte, size*sizeof(ffm_float));
    if(status != 0)
        throw bad_alloc();
#endif
    
    return (ffm_float*)ptr;
}

inline void init_sign(ffm_float *sign)
{
#if defined FM_STANDARD
    sign[0] = sign[2] = 1.0f;
    sign[1] = sign[3] = 1.0f;
#elif defined FM_NEUTRAL
    sign[0] = sign[2] = 1.0f;
    sign[1] = sign[3] = -1.0f;
#elif defined FM_REVERSED
    sign[0] = sign[2] = -1.0f;
    sign[1] = sign[3] = -1.0f;
#else
    error_FM_STANDARD_or_FM_NEUTRAL_or_FM_REVERSED_must_be_defined
#endif
}

ffm_model* init_model(ffm_int n, ffm_int m, ffm_parameter param)
{
    ffm_int k_aligned = (ffm_int)ceil((ffm_double)param.k/kALIGN)*kALIGN;

    ffm_model *model = new ffm_model;
    model->n = n;
    model->k = k_aligned;
    model->m = m;
    model->W = nullptr;
    model->sign = nullptr;
    model->normalization = param.normalization;
    
    try
    {
        model->W = malloc_aligned_float((ffm_long)n*k_aligned*2);
#ifdef FM_USE_LINEAR
        model->WL = malloc_aligned_float(n*2);
        model->WB = malloc_aligned_float(2);
#endif
        model->sign = malloc_aligned_float(4);
    }
    catch(bad_alloc const &e)
    {
        ffm_destroy_model(&model);
        throw;
    }

    ffm_float *sign = model->sign;
    init_sign(sign);

    ffm_float coef = 1.0f/sqrt(param.k) * param.sigma;
    ffm_float *w = model->W;

    default_random_engine generator;
    uniform_real_distribution<ffm_float> distribution(0.0, 1.0);

    for(ffm_int j = 0; j < model->n; j++)
    {
        for(ffm_int d = 0; d < param.k; d++, w++)
            *w = coef*distribution(generator);
        for(ffm_int d = param.k; d < k_aligned; d++, w++)
            *w = 0;
        for(ffm_int d = k_aligned; d < 2*k_aligned; d++, w++)
            *w = 1;
    }

#ifdef FM_USE_LINEAR
    for(ffm_int j = 0; j < model->n; j++)
    {
        model->WL[j * 2] = 0;
        model->WL[j * 2 + 1] = 1;
    }

    model->WB[0] = 0;
    model->WB[1] = 1;
#endif

    return model;
}

void shrink_model(ffm_model &model, ffm_int k_new)
{
    for(ffm_int j = 0; j < model.n; j++)
    {
        ffm_float *src = model.W + (ffm_long)j*model.k*2;
        ffm_float *dst = model.W + (ffm_long)j*k_new;
        copy(src, src+k_new, dst);
    }

#ifdef FM_USE_LINEAR
    for(ffm_int j = 0; j < model.n; j++)
    {
        ffm_float *src = model.WL + (ffm_long)j*2;
        ffm_float *dst = model.WL + (ffm_long)j;
        copy(src, src+1, dst);
    }

    for(ffm_int j = 0; j < 1; j++)
    {
        ffm_float *src = model.WB + (ffm_long)j*2;
        ffm_float *dst = model.WB + (ffm_long)j;
        copy(src, src+1, dst);
    }
#endif

    model.k = k_new;
}

vector<ffm_float> normalize(ffm_problem &prob)
{
    vector<ffm_float> R(prob.l);
#if defined USEOMP
#pragma omp parallel for schedule(static)
#endif
    for(ffm_int i = 0; i < prob.l; i++)
    {
        ffm_float norm = 0;
        for(ffm_long p = prob.P[i]; p < prob.P[i+1]; p++)
            norm += prob.X[p].v*prob.X[p].v;
        R[i] = 1 / sqrt(norm);
    }

    return R;
}

shared_ptr<ffm_model> train(
    ffm_problem *tr, 
    vector<ffm_int> &order, 
    ffm_parameter param, 
    ffm_problem *va=nullptr)
{
#if defined USEOMP
    ffm_int old_nr_threads = omp_get_num_threads();
    omp_set_num_threads(param.nr_threads);
#endif

    shared_ptr<ffm_model> model = 
        shared_ptr<ffm_model>(init_model(tr->n, tr->m, param),
            [] (ffm_model *ptr) { ffm_destroy_model(&ptr); });

    vector<ffm_float> R_tr, R_va;
    if(param.normalization)
    {
        R_tr = normalize(*tr);
        if(va != nullptr)
            R_va = normalize(*va);
    }
    else
    {
        R_tr = vector<ffm_float>(tr->l, 1);
        if(va != nullptr)
            R_va = vector<ffm_float>(va->l, 1);
    }

    bool auto_stop = param.auto_stop && va != nullptr && va->l != 0;

    ffm_int k_aligned = (ffm_int)ceil((ffm_double)param.k/kALIGN)*kALIGN;
    ffm_long w_size = (ffm_long)model->n * k_aligned * 2;
    vector<ffm_float> prev_W;
    if(auto_stop)
        prev_W.assign(w_size, 0);
    ffm_double best_va_loss = numeric_limits<ffm_double>::max();

    if(!param.quiet)
    {
        if(param.auto_stop && (va == nullptr || va->l == 0))
            cerr << "warning: ignoring auto-stop because there is no validation set" << endl;

        cout.width(4);
        cout << "iter";
        cout.width(13);
        cout << "tr_logloss";
        if(va != nullptr && va->l != 0)
        {
            cout.width(13);
            cout << "va_logloss";
        }
        cout << endl;
    }
    
    Timer timer;

    for(ffm_int iter = 1; iter <= param.nr_iters; iter++)
    {
        ffm_double tr_loss = 0;
        if(param.random)
            random_shuffle(order.begin(), order.end());

        timer.tic();
#ifdef FM_CHECK_GRADIENTS
ffm_int incorrect_gradients = 0;
ffm_int incorrect_gradient_signs = 0;
ffm_int total_gradients = 0;
#endif
#if defined USEOMP
#pragma omp parallel for schedule(static) reduction(+: tr_loss)
#endif
        for(ffm_int ii = 0; ii < tr->l; ii++)
        {
            ffm_int i = order[ii];

            ffm_float y = tr->Y[i];
            
            ffm_node *begin = &tr->X[tr->P[i]];

            ffm_node *end = &tr->X[tr->P[i+1]];

            ffm_float r = R_tr[i];

            ffm_float t = wTx(begin, end, r, *model);

            ffm_float kappa;

            if(param.classification)
            {
                ffm_float expnyt = exp(-y*t);

                tr_loss += log(1+expnyt);

                kappa = -y*expnyt/(1+expnyt);
            } else {
                ffm_float e = y - t;

                tr_loss += e * e;

                kappa = -e;
            }

#ifdef FM_CHECK_GRADIENTS
            bool kill_kappa = false;
            check_gradients(*model, begin, end, kappa, param, y, r, w_size, iter, ii, kill_kappa, incorrect_gradients, total_gradients, incorrect_gradient_signs);
#endif

            wTx(begin, end, r, *model, kappa, param.eta, param.lambda, true);
        }

#ifdef FM_CHECK_GRADIENTS
        cout << setprecision(5) << std::fixed;
        cout << incorrect_gradients << " / " << total_gradients << " = " << incorrect_gradients / 1.0f / total_gradients << " incorrect gradients" << endl;
        cout << incorrect_gradient_signs << " / " << total_gradients << " = " << incorrect_gradient_signs / 1.0f / total_gradients << " incorrect gradient signs" << endl;
#endif

        if(!param.quiet)
        {
            tr_loss /= tr->l;

            cout.width(4);
            cout << iter;
            cout.width(13);
            cout << fixed << setprecision(5) << (param.classification ? tr_loss : sqrt(tr_loss));
            if(va != nullptr && va->l != 0)
            {
                ffm_double va_loss = 0;
#if defined USEOMP
#pragma omp parallel for schedule(static) reduction(+:va_loss)
#endif
                for(ffm_int i = 0; i < va->l; i++)
                {
                    ffm_float y = va->Y[i];

                    ffm_node *begin = &va->X[va->P[i]];

                    ffm_node *end = &va->X[va->P[i+1]];

                    ffm_float r = R_va[i];

                    ffm_float t = wTx(begin, end, r, *model);
                    
                    if(param.classification)
                    {
                        ffm_float expnyt = exp(-y*t);

                        va_loss += log(1+expnyt);
                    } else {
                        ffm_float e = y - t;

                        va_loss += e * e;
                    }
                }
                va_loss /= va->l;

                cout.width(13);
                cout << fixed << setprecision(5) << (param.classification ? va_loss : sqrt(va_loss));

                if(auto_stop)
                {
                    if(va_loss > best_va_loss)
                    {
                        memcpy(model->W, prev_W.data(), w_size*sizeof(ffm_float));
                        cout << endl << "Auto-stop. Use model at " << iter-1 << "th iteration." << endl;
                        break;
                    }
                    else
                    {
                        memcpy(prev_W.data(), model->W, w_size*sizeof(ffm_float));
                        best_va_loss = va_loss; 
                    }
                }
            }
            cout << "    " << timer.toc() << endl;
        }
    }

    shrink_model(*model, param.k);

#if defined USEOMP
    omp_set_num_threads(old_nr_threads);
#endif

    return model;
}

} // unnamed namespace

ffm_problem* ffm_read_problem(char const *path, bool classification)
{
    if(strlen(path) == 0)
        return nullptr;

    FILE *f = fopen(path, "r");
    if(f == nullptr)
        return nullptr;

    ffm_problem *prob = new ffm_problem;
    prob->l = 0;
    prob->n = 0;
    prob->m = 0;
    prob->X = nullptr;
    prob->P = nullptr;
    prob->Y = nullptr;

    char line[kMaxLineSize];

    ffm_long nnz = 0;
    for(; fgets(line, kMaxLineSize, f) != nullptr; prob->l++)
    {
        strtok(line, " \t");
        for(; ; nnz++)
        {
            char *ptr = strtok(nullptr," \t");
            if(ptr == nullptr || *ptr == '\n')
                break;
        }
    }
    rewind(f);

    prob->X = new ffm_node[nnz];
    prob->P = new ffm_long[prob->l+1];
    prob->Y = new ffm_float[prob->l];
    const int maxN = 20000000;
    ffm_float *feature_prob = new ffm_float[maxN];
    for(ffm_int idx = 0; idx < maxN; idx++)
    {
        feature_prob[idx] = 0.0f;
    }

    ffm_long p = 0;
    prob->P[0] = 0;
    ffm_int num_rows = 0;
    for(ffm_int i = 0; fgets(line, kMaxLineSize, f) != nullptr; i++)
    {
        num_rows++;
        char *y_char = strtok(line, " \t");
        ffm_float y = classification ? ((atoi(y_char)>0)? 1.0f : -1.0f) : atof(y_char);
        prob->Y[i] = y;

        for(; ; p++)
        {
            char *field_char = strtok(nullptr,":");
            char *idx_char = strtok(nullptr,":");
            char *value_char = strtok(nullptr," \t");
            if(field_char == nullptr || *field_char == '\n')
                break;

            ffm_int field = atoi(field_char);
            ffm_int idx = atoi(idx_char);
            ffm_float value = atof(value_char);

            prob->m = max(prob->m, field+1);
            prob->n = max(prob->n, idx+1);
            
            prob->X[p].f = field;
            prob->X[p].j = idx;
            prob->X[p].v = value;
            feature_prob[idx] += 1.0;
        }
        prob->P[i+1] = p;
    }

    ffm_float min_prob = 1.0f;
    ffm_float max_prob = 0.0f;
    for(ffm_int idx = 0; idx < prob->n; idx++)
    {
        feature_prob[idx] /= num_rows;
        if(feature_prob[idx] > 0.0f)
        {
            min_prob = min(min_prob, feature_prob[idx]);
        }
        max_prob = max(max_prob, feature_prob[idx]);
        //cerr << idx << " feature_prob = " << feature_prob[idx] << endl;
    }
    //cerr << "min probability of a feature appearing " << min_prob << endl;
    //cerr << "max probability of a feature appearing " << max_prob << endl;
    ffm_float min_prob_clip = 1e-5f;
    //cerr << "clipping min feature prob at " << min_prob_clip << endl;

    for(ffm_long i = 0; i < p; i++)
    {
        prob->X[i].feature_prob = feature_prob[(prob->X[i]).j];
        prob->X[i].feature_prob = max(prob->X[i].feature_prob, min_prob_clip);
        prob->X[i].feature_prob = 1.0f;
    }
    delete[] feature_prob;

    fclose(f);

    return prob;
}

int ffm_read_problem_to_disk(char const *txt_path, char const *bin_path)
{
    exit(1);// on_disk is broken 
    return 0;
}

void ffm_destroy_problem(ffm_problem **prob)
{
    if(prob == nullptr || *prob == nullptr)
        return;
    delete[] (*prob)->X;
    delete[] (*prob)->P;
    delete[] (*prob)->Y;
    delete *prob;
    *prob = nullptr;
}

ffm_int ffm_save_model(ffm_model *model, char const *path)
{
    ofstream f_out(path);
    if(!f_out.is_open())
        return 1;

    f_out << "n " << model->n << "\n";
    f_out << "m " << model->m << "\n";
    f_out << "k " << model->k << "\n";
    f_out << "normalization " << model->normalization << "\n";

    ffm_float *ptr = model->W;
    for(ffm_int j = 0; j < model->n; j++)
    {
        f_out << "w" << j << " ";
        for(ffm_int d = 0; d < model->k; d++, ptr++)
            f_out << *ptr << " ";
        f_out << "\n";
    }

#ifdef FM_USE_LINEAR
    ptr = model->WL;
    for(ffm_int j = 0; j < model->n; j++, ptr++)
    {
        f_out << "wl" << j << " " << *ptr << " \n";
    }

    ptr = model->WB;
    f_out << "wb " << *ptr << " \n";
#endif

    return 0;
}

ffm_model* ffm_load_model(char const *path)
{
    ifstream f_in(path);
    if(!f_in.is_open())
        return nullptr;

    string dummy;

    ffm_model *model = new ffm_model;
    model->W = nullptr;
#ifdef FM_USE_LINEAR
    model->WL = nullptr;
    model->WB = nullptr;
#endif
    model->sign = nullptr;

    f_in >> dummy >> model->n >> dummy >> model->m >> dummy >> model->k 
         >> dummy >> model->normalization;

    try
    {
        model->W = malloc_aligned_float((ffm_long)model->m*model->n*model->k);
#ifdef FM_USE_LINEAR
        model->WL = malloc_aligned_float((ffm_long)model->n);
        model->WB = malloc_aligned_float(1);
#endif
        model->sign = malloc_aligned_float(4);
    }
    catch(bad_alloc const &e)
    {
        ffm_destroy_model(&model);
        return nullptr;
    }

    ffm_float *sign = model->sign;
    init_sign(sign);

    ffm_float *ptr = model->W;
    for(ffm_int j = 0; j < model->n; j++)
    {
        f_in >> dummy;
        for(ffm_int d = 0; d < model->k; d++, ptr++)
            f_in >> *ptr;
    }

#ifdef FM_USE_LINEAR
    ptr = model->WL;
    for(ffm_int j = 0; j < model->n; j++, ptr++)
    {
        f_in >> dummy >> *ptr;
    }

    ptr = model->WB;
    f_in >> dummy >> *ptr;
#endif

    return model;
}

void ffm_destroy_model(ffm_model **model)
{
    if(model == nullptr || *model == nullptr)
        return;
#ifdef _WIN32
    _aligned_free((*model)->W);
#ifdef FM_USE_LINEAR
    _aligned_free((*model)->WL);
    _aligned_free((*model)->WB);
#endif
    _aligned_free((*model)->sign);
#else
    free((*model)->W);
#ifdef FM_USE_LINEAR
    free((*model)->WL);
    free((*model)->WB);
#endif
    free((*model)->sign);
#endif
    delete *model;
    *model = nullptr;
}

ffm_parameter ffm_get_default_param()
{
    ffm_parameter param;

    param.eta = 0.2;
    param.lambda = 0.00002;
    param.nr_iters = 15;
    param.k = 4;
    param.nr_threads = 1;
    param.quiet = false;
    param.normalization = true;
    param.random = true;
    param.auto_stop = false;
    param.classification = true;
    param.sigma = 1.0f;

    return param;
}

ffm_model* ffm_train_with_validation(ffm_problem *tr, ffm_problem *va, ffm_parameter param)
{
    vector<ffm_int> order(tr->l);
    for(ffm_int i = 0; i < tr->l; i++)
        order[i] = i;

    shared_ptr<ffm_model> model = train(tr, order, param, va);

    ffm_model *model_ret = new ffm_model;

    model_ret->n = model->n;
    model_ret->m = model->m;
    model_ret->k = model->k;
    model_ret->normalization = model->normalization;

    model_ret->W = model->W;
#ifdef FM_USE_LINEAR
    model_ret->WL = model->WL;
    model_ret->WB = model->WB;
#endif
    model_ret->sign = model->sign;
    model->W = nullptr;
#ifdef FM_USE_LINEAR
    model->WL = nullptr;
    model->WB = nullptr;
#endif
    model->sign = nullptr;

    return model_ret;
}

ffm_model* ffm_train(ffm_problem *prob, ffm_parameter param)
{
    return ffm_train_with_validation(prob, nullptr, param);
}

ffm_model* ffm_train_with_validation_on_disk(
    char const *tr_path,
    char const *va_path,
    ffm_parameter param)
{
    exit(1);// on_disk is broken
}

ffm_model* ffm_train_on_disk(char const *prob_path, ffm_parameter param)
{
    exit(1);// on_disk is broken
    return ffm_train_with_validation_on_disk(prob_path, "", param);
}

ffm_float ffm_predict(ffm_node *begin, ffm_node *end, ffm_model *model, bool classification)
{
    ffm_float r = 1;
    if(model->normalization)
    {
        r = 0;
        for(ffm_node *N = begin; N != end; N++)
            r += N->v*N->v; 
        r = 1 / sqrt(r);
    }

    ffm_float t = wTx(begin, end, r, *model, 0, 0, 0, false, 1);

    return classification ? 1/(1+exp(-t)) : t;
}

ffm_float ffm_cross_validation(
    ffm_problem *prob, 
    ffm_int nr_folds,
    ffm_parameter param)
{
#if defined USEOMP
    ffm_int old_nr_threads = omp_get_num_threads();
    omp_set_num_threads(param.nr_threads);
#endif

    bool quiet = param.quiet;
    param.quiet = true;

    vector<ffm_int> order(prob->l);
    for(ffm_int i = 0; i < prob->l; i++)
        order[i] = i;
    random_shuffle(order.begin(), order.end());

    if(!quiet)
    {
        cout.width(4);
        cout << "fold";
        cout.width(13);
        cout << "logloss";
        cout << endl;
    }

    ffm_double loss = 0;
    ffm_int nr_instance_per_fold = prob->l/nr_folds;
    for(ffm_int fold = 0; fold < nr_folds; fold++)
    {
        ffm_int begin = fold*nr_instance_per_fold;
        ffm_int end = min(begin + nr_instance_per_fold, prob->l);

        vector<ffm_int> order1;
        for(ffm_int i = 0; i < begin; i++)
            order1.push_back(order[i]);
        for(ffm_int i = end; i < prob->l; i++)
            order1.push_back(order[i]);

        shared_ptr<ffm_model> model = train(prob, order1, param);

        ffm_double loss1 = 0;
#if defined USEOMP
#pragma omp parallel for schedule(static) reduction(+: loss1)
#endif
        for(ffm_int ii = begin; ii < end; ii++)
        {
            ffm_int i = order[ii];

            ffm_float y = prob->Y[i];
            
            ffm_node *begin = &prob->X[prob->P[i]];

            ffm_node *end = &prob->X[prob->P[i+1]];

            ffm_float y_bar = ffm_predict(begin, end, model.get(), param.classification);

            loss1 -= param.classification ? (y==1? log(y_bar) : log(1-y_bar)) : - (y - y_bar) * (y - y_bar);
        }
        loss += loss1;

        if(!quiet)
        {
            cout.width(4);
            cout << fold;
            cout.width(13);
            cout << fixed << setprecision(4) << loss1 / (end-begin);
            cout << endl;
        }
    }

    if(!quiet)
    {
        cout.width(17);
        cout.fill('=');
        cout << "" << endl;
        cout.fill(' ');
        cout.width(4);
        cout << "avg";
        cout.width(13);
        cout << fixed << setprecision(4) << (param.classification ? loss/prob->l : sqrt(loss/prob->l));
        cout << endl;
    }

#if defined USEOMP
    omp_set_num_threads(old_nr_threads);
#endif

    return (param.classification ? loss/prob->l : sqrt(loss/prob->l));
}

} // namespace ffm

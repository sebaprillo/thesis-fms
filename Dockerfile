FROM ubuntu:18.04

ENV http_proxy http://proxy.fcen.uba.ar:8080
ENV https_proxy http://proxy.fcen.uba.ar:8080

RUN apt-get clean && apt-get update -y && apt-get install -y curl git build-essential screen nano unzip vim parallel bc libgl1-mesa-glx time g++ wget bzip2 gdb texlive-latex-base texlive-science texlive-lang-spanish
RUN useradd condauser -m

USER condauser

WORKDIR /home/condauser
ENV HOME /home/condauser

RUN curl -O https://repo.anaconda.com/archive/Anaconda3-5.3.0-Linux-x86_64.sh && bash Anaconda3-5.3.0-Linux-x86_64.sh -b -p 

USER root
RUN ln -s /home/condauser/anaconda3/etc/profile.d/conda.sh /etc/profile.d/conda.sh 
RUN apt-get -y install libxrender1

USER condauser
WORKDIR /home/condauser

RUN echo ". /home/condauser/anaconda3/etc/profile.d/conda.sh" >> ~/.bashrc
RUN echo "conda activate" >> ~/.bashrc

ENV PATH="/home/condauser/anaconda3/bin:$PATH"
RUN conda update -n base conda
RUN conda install -c conda-forge -y pandas scikit-learn seaborn jupyterlab
RUN pip install kaggle xgboost lightgbm

EXPOSE 8888
CMD ["jupyter", "lab", "--no-browser", "--config=~/work/.jupyter/jupyter_notebook_config.json"]
